package com.stocktastic;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Stock;

public class StockHandlerTest {

	public static String TICKERFILE = "testdata/tickers.txt";
	public static String STOCKCOLLECTIONFILE = "testdata/stockcollections.txt";
	public static String FILESLOCATION = "testdata/";
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	// SUT
	private StockHandler testStockHandler;
	
	public StockHandlerTest() {
	}

	@Before
	public void setUp() throws Exception {
		testStockHandler = new StockHandler(TICKERFILE, STOCKCOLLECTIONFILE, FILESLOCATION);
		
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}

	@Test
	public void testLoadTickerFile() {
		testStockHandler.loadTickerFile();
		Stock testStock = testStockHandler.getStock("Tieto Corp");
		assertNotNull("We have a ticker file loaded", testStock);
		assertNotNull("StockHandler created successfully", testStockHandler);
	}
	@Rule public ExpectedException thrown = ExpectedException.none();
	
	@Test(expected= FileNotFoundException.class)
	public void testLoadTickerFileIncorrectPath() throws Exception {
		StockHandler testStockHandler2 = new StockHandler("testdata/incorrect_path_tickers.txt", STOCKCOLLECTIONFILE, FILESLOCATION);
		testStockHandler2.loadTickerFile();
	}

	@Test
	public void testFindOneStock() {
		testStockHandler.loadTickerFile();

		List<String> searchTerms = new ArrayList<String>();
		searchTerms.add("Tieto");
		List<String> foundStocks = testStockHandler.findStocks(searchTerms);
		
		List<String> expectedStocks = new ArrayList<String>();
		expectedStocks.add("Tieto Corp");
		assertEquals(expectedStocks, foundStocks);
	}

	@Test
	public void testFindMultipleStocks() {
		testStockHandler.loadTickerFile();

		List<String> searchTerms = new ArrayList<String>();
		searchTerms.add("Tieto");
		searchTerms.add("Swedbank AB");
		List<String> foundStocks = testStockHandler.findStocks(searchTerms);
		
		List<String> expectedStocks = new ArrayList<String>();
		expectedStocks.add("Tieto Corp");
		expectedStocks.add("Swedbank AB");
		
		assertEquals(expectedStocks, foundStocks);
	}

	
	@Test
	public void testGetStock() {
		testStockHandler.loadTickerFile();

		Stock expectedStock = new Stock("Tieto Corp", "TIEN.ST");
		//expectedStock.setPrices( getTestPriceHistory("TIEN.ST") );
		
		Stock actualStock = testStockHandler.getStock("Tieto Corp");
		//actualStock.setPrices( getTestPriceHistory("TIEN.ST") );
		
		assertNotNull(actualStock);
		assertThat("Did not get the correct stock back", actualStock, is(equalTo(expectedStock)));
		assertEquals("Did not get the correct stock back", expectedStock, actualStock);
	}
	

	@Test
	public void testGetStockNotFound() {
		 
		testStockHandler.loadTickerFile();

		Stock actualStock = testStockHandler.getStock("Notvalid Corp");
		assertNull(actualStock);
		assertEquals("The stock with name Notvalid Corp could not be found\n! Please try again!", outContent.toString());
	}
	
	
	@Test
	public void testGetStockIncorrectParsePriceListings() {
		 
		testStockHandler.loadTickerFile();

		Stock actualStock = testStockHandler.getStock("Incorrect Parse Corp");
		assertEquals("There was a problem when parsing the data read from file. " +
				     " Please check the source file for the selected stock. File " +
				     "location: testdata/SKA-B-INCORRECT-FORMAT.ST.csv\n", 
				     outContent.toString());
	}

	
	@Test
	public void testGetStockIncorrectPriceListingsFileFormat() {
		 
		testStockHandler.loadTickerFile();

		Stock actualStock = testStockHandler.getStock("Incorrect Format Corp");
		assertEquals("There was a problem when parsing the data read from file. " +
			         " Please check the source file for the selected stock. File " +
			         "location: testdata/index.jpeg.csv\n", 
				     outContent.toString());
	}
	
	
	@Test
	public void testGetStocks() {
		testStockHandler.loadTickerFile();

		ArrayList<Stock> expectedStocks = new ArrayList<Stock>();
		
		Stock expectedStock = new Stock("Tieto Corp", "TIEN.ST");
		expectedStocks.add(expectedStock);
		Stock expectedStock2 = new Stock("Svenska Handelsbanken", "SHB-B.ST");
		expectedStocks.add(expectedStock2);
		Stock expectedStock3 = new Stock("Skanska AB", "SKA-B.ST");
		expectedStocks.add(expectedStock3);

		List<String> searchedStocks = new ArrayList<String>();
		searchedStocks.add("Tieto Corp");
		searchedStocks.add("Svenska Handelsbanken");
		searchedStocks.add("Skanska AB");
		
		ArrayList<Stock> actualStocks = (ArrayList<Stock>)testStockHandler.getStocks(searchedStocks);
		
		assertEquals(expectedStocks, actualStocks);
	}
	
	/**
	 * Loads testdata into a PriceListingsArray
	 * @param symbolName
	 */
	private ArrayList<PriceListing> getTestPriceHistory(String symbolName) {
		
		/**
		 * Load test data stock price listings into a priceListings array from a test file
		 */
		BufferedReader inReader = null;
		File priceHistoryFile = new File(FILESLOCATION + symbolName + ".csv");
		ArrayList<PriceListing> priceListings = new ArrayList<PriceListing>();
		
		try {
			inReader = new BufferedReader(new FileReader(priceHistoryFile));
			String inLine = null;
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			while ((inLine = inReader.readLine()) != null) {
				
				if (!inLine.matches("^Date.*")) {
					String[] splitLine = inLine.split(",");
					priceListings.add(new PriceListing(dateFormat.parse(splitLine[0]), 
									  Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), 
									  Float.parseFloat(splitLine[3]), Float.parseFloat(splitLine[4]), 
									  Float.parseFloat(splitLine[6]), Integer.parseInt(splitLine[5])));
				} 
			}
			Collections.sort(priceListings);
		} 
		catch (NumberFormatException e) {
			System.out.format("There is an incorrectly formated value in the data read from file. "
							+ " Please check the source file for the selected stock. File location: %s%n",
							priceHistoryFile);
		} 
		catch (ParseException e) {
			System.out.format("There was a problem when parsing the data read from file. "
					+ " Please check the source file for the selected stock. File location: %s%n",
					priceHistoryFile);
		}
		catch (IOException e) {
			System.out.format("There was an IO-problem when reading the price data file: %s%n", 
							  priceHistoryFile);
		}
		finally {
			try {
				inReader.close();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return priceListings;
	} // end getTestPriceHistory
	
}
