package com.stocktastic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.stocktastic.pojo.Index;
import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Stock;

/**
 * Class that handles Index objects. Creating Index objects
 * from file, return Index collections etc.
 * 
 * @author stationaradm
 */
public class IndexHandler {
	
	// File that contains ticker information.
	private File indexFile;
	
	private BufferedReader buffReader;
	
	// Map that contains the name of the company as key and
	// the symbol name as a value.
	private Map<String, String> indexNameMap;
	
	private String priceHistoryLocation;
	
	/**
	 * Constructor for class StockHandler
	 * 
	 * @param tickerFilePath a String that is the path to a textfile containing ticker symbol names
	 * @param filesLocation a String that is the path to the folder where data is kept for stocktastic
	 */
	public IndexHandler(String argIndexFilePath, String argPriceHistoryLocation) {
		indexNameMap = new HashMap<String, String>();
		this.indexFile = new File(argIndexFilePath);
		this.priceHistoryLocation = argPriceHistoryLocation;
	} // end constructor StockHandler()


	/**
	 * Loads the ticker file containing all the company names mapped
	 * to symbol names.
	 */
	public void loadIndexFile() {

		try {
			buffReader = new BufferedReader(new FileReader(indexFile));
			String priceLine = null;
			String[] indexArray = null;
			
			while ((priceLine = buffReader.readLine()) != null) {
				
				// Delimeter in downloaded file is 1 Space + Tab for some reason.
				indexArray = priceLine.split("	");
				indexNameMap.put(indexArray[0], indexArray[1]);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.format("The file containing indices is not available: %s", indexFile.getAbsolutePath() );
		}
		catch (IOException e) {
			System.out.format("Failed to read indicies file: %s", indexFile.getAbsolutePath());
		}
		finally {
			try {
				if (buffReader != null) {
					buffReader.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	} // end loadTIckerFile()
	
	
	/**
	 * Retrieves matches to each occurence of the search words list given 
	 * as argument to the method. Each search word is compared with regular 
	 * expression to the existing index names.
	 * 
	 * @param searchedIndices a List of Strings which hold corporate names
	 * @return a List of stocks that match the given search words.
	 */
	public List<String> findIndices(List<String> argSearchedIndices) {
		
		List<String> foundIndexList = new ArrayList<String>();
		
		for (String searchWord : argSearchedIndices) {
			for (String currentKey : indexNameMap.keySet()) {
				if (currentKey.matches("(?i:.*" + searchWord + ".*)")) {
					foundIndexList.add(currentKey);
				}
			}
		}
		
		return foundIndexList;
	} // end findIndices()
	
	/**
	 * Returns all available indices for download
	 * 
	 * @return a List with the name of all indices available
	 */
	public List<String> getAllIndices() {

		List<String> allIndicesList = new ArrayList<String>();
		
		for (String currentKey : indexNameMap.keySet()) {
			allIndicesList.add(currentKey);
		}
		
		return allIndicesList;
	} // end getAllIndices()
	
	/**
	 * Prints all the tickers available.
	 */
	public void printIndexList() {
		
		Map<String, String> sortedIndexMap = new TreeMap<String, String>(indexNameMap);
		
		for (Entry<String, String> entrySet : sortedIndexMap.entrySet()) {
			System.out.format("%s : %s%n", entrySet.getKey(), entrySet.getValue());
		}
		
	} // end printIndexList()
	

	/**
	 * Returns a single index with the specified name
	 * 
	 * @param argIndexName a String that is the ticker name for an index
	 * @return a Index object that represents the selected index
	 */
	public Index getIndex(String argIndexName) {
		
		Index selectedIndex = null;
		boolean chosenIndexOk = true;
		BufferedReader inReader = null;
		// Check that the stock exists
			
		if (!indexNameMap.containsKey(argIndexName)) {
			chosenIndexOk = false;
			System.out.format("The stock with name %s could not be found%n! Please try again!", argIndexName);
		}
		else {
			if (System.getenv("DEBUG").equals("true")) {
				System.out.format("Index %s with symbol %s found!%n", argIndexName, indexNameMap.get(argIndexName));
			}
			selectedIndex = new Index(argIndexName, indexNameMap.get(argIndexName));
			File priceHistoryFile = new File(priceHistoryLocation + indexNameMap.get(argIndexName) + ".csv");
			ArrayList<PriceListing> priceListings = new ArrayList<PriceListing>();
			
			if (priceHistoryFile.exists()) {
				
				try {
					inReader = new BufferedReader(new FileReader(priceHistoryFile));
					String inLine = null;
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

					while ((inLine = inReader.readLine()) != null) {
						
						if (!inLine.matches("^Date.*")) {
							String[] splitLine = inLine.split(",");
							priceListings.add(new PriceListing(dateFormat.parse(splitLine[0]), 
											  Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), 
											  Float.parseFloat(splitLine[3]), Float.parseFloat(splitLine[4]), 
											  Float.parseFloat(splitLine[6]), Integer.parseInt(splitLine[5])));
						} 
					}
					Collections.sort(priceListings);
					selectedIndex.setPrices(priceListings);
				} 
				catch (ParseException e) {
					System.out.format("There was a problem when parsing the data read from file. "
							+ " Please check the source file for the selected index. File location: %s%n",
							priceHistoryFile);
				}
				catch (IOException e) {
					System.out.format("There was an IO-problem when reading the price data file: %s%n", 
									  priceHistoryFile);
				}
				finally {
					if (inReader != null) {
						try {
							inReader.close();
						} 
						catch (IOException e) {
							e.printStackTrace();
						}
						
					}
				}
			}
		}
		
		return selectedIndex;
	} // end getIndex()
	
	
	/**
	 * Returns a list of indices with the supplied names
	 * 
	 * @return a List of indces that has been fetched from file.
	 */
	public List<Index> getIndices(List<String> argIndexNames) {
		
		List<Index> indexList = new ArrayList<Index>();
		boolean chosenIndexOk = true;
		BufferedReader inReader = null;
		// Check that the stock exists
		for (String indexName : argIndexNames) {
			
			if (!indexNameMap.containsKey(indexName)) {
				chosenIndexOk = false;
				System.out.println("The stock could not be found! Please try again!");
				break;
			}
			else {
				System.out.format("Stock %s with symbol %s found!%n", indexName, indexNameMap.get(indexName));
				Index currentIndex = new Index(indexName, indexNameMap.get(argIndexNames));
				File priceHistoryFile = new File(priceHistoryLocation + indexNameMap.get(indexName) + ".csv");
				ArrayList<PriceListing> priceListings = new ArrayList<PriceListing>();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

				try {
					inReader = new BufferedReader(new FileReader(priceHistoryFile));
					String inLine = null;
				
					while ((inLine = inReader.readLine()) != null) {
						
						if (!inLine.matches("^Date.*")) {
							String[] splitLine = inLine.split(",");
							priceListings.add(new PriceListing(dateFormat.parse(splitLine[0]), 
											  Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), 
											  Float.parseFloat(splitLine[3]), Float.parseFloat(splitLine[4]), 
											  Float.parseFloat(splitLine[6]), Integer.parseInt(splitLine[5])));
						} 
					}
					Collections.sort(priceListings);
					Collections.reverse(priceListings);
					currentIndex.setPrices(priceListings);
					indexList.add(currentIndex);
				} 
				catch (NumberFormatException e) {
					e.printStackTrace();
				} 
				catch (ParseException e) {
					e.printStackTrace();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					if (inReader != null) {
						try {
							inReader.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return indexList;
		
	} // end getIndices()
	
	// TODO Method that searches for a specific stock.
	
} // end class StockHandler
