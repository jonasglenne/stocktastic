package com.stocktastic;

import java.util.List;
import java.util.Map;
import com.stocktastic.pojo.Stock;

/**
 * Interface for classes using Stock objects. Creating Stock objects
 * from file, return Stock collections etc.
 * 
 * @author stationaradm
 */
public interface IStockHandler {
	
	/**
	 * Loads the ticker file containing all the company names mapped
	 * to symbol names.
	 */
	public void loadTickerFile();	
	
	/**
	 * Retrieves matches to each occurence of the search words list given 
	 * as argument to the method. Each search word is compared with regular 
	 * expression to the existing corporation names.
	 * 
	 * @param searchedStocks a List of Strings which hold corporate names
	 * @return a List of stocks that match the given search words.
	 */
	public List<String> findStocks(List<String> searchedStocks);
	
	
	/**
	 * Prints all the tickers available.
	 */
	public void printTickerList();

	/**
	 * Returns a single stock with the specified name
	 * 
	 * @param stockName a String that is the ticker name for the stock
	 * @return a Stock object that represents the selected stock
	 */
	public Stock getStock(String stockName);	
	
	/**
	 * Returns a list of stocks with the supplied names
	 * 
	 * @return a List of stocks that has been fetched from file.
	 */
	public List<Stock> getStocks(List<String> stockNames);	
	
	/**
	 * Loads all stock collections (user defined and official, e.g. OMXS30) into 
	 * the stock handler. The collections are stored in a regular text file separated
	 * by ','.
	 */
	public void loadStockCollections();
	
	/**
	 * Checks if the specified collection can be found in the map.
	 * 
	 * @param collectionName a String that is the specified collection name
	 * @return a boolean that is true if the specified collection can be found
	 */
	public boolean containsCollection(String collectionName);
	
	/**
	 * Returns a map of all individual stock components of the specified list.
	 * 
	 * @param collectionName a String that specifies the stock collection (e.g. OMXS30)
	 * @return a Map containing company name as key and stock symbol as value
	 */
	public Map<String, String> getStockCollection(String collectionName);
	
} // end class StockHandler
