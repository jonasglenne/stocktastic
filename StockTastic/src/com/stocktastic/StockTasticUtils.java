package com.stocktastic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.stocktastic.pojo.PriceListing;

/**
 * Class that have utility functions for stocktastic application
 * 
 * @author stationaradm
 */
public class StockTasticUtils {
	
	/**
	 * Return today's date with hours, minutes and seconds set to 0.
	 * 
	 * @return a Date that is set to today's date
	 */
	public static Date getCurrentDate() {
		
		Calendar todayCal = new GregorianCalendar();
		todayCal.set(Calendar.HOUR_OF_DAY, 0);  
		todayCal.set(Calendar.MINUTE, 0);  
		todayCal.set(Calendar.SECOND, 0);  
		todayCal.set(Calendar.MILLISECOND, 0);
		
		return todayCal.getTime();	
	} // end getCurrentDate()
	
	/**
	 * Calculates the difference in days between the current date and an earlier date.
	 * 
	 * @param argCurrDate a Date that represents a current date
	 * @param argEarlierDate a Date that represents an earlier date
	 *
	 * @return an int that is the difference in days between the current date and the earlier date
	 */
	public static long calcDateDayDifference(Date argCurrDate, Date argEarlierDate) {
		return (argCurrDate.getTime() - argEarlierDate.getTime()) / (1000 * 60 * 60 *24);
	} // end calcDateDayDifference()
		
	/**
	 * Calculate Simple Moving Average (SMA) values for a stocks price history
	 * 
	 * @param argPriceHist an ArrayList<PriceHistory> that contains price history for a stock.
	 * @param argMaPeriod an int that is the number of days in the MA period
	 */
	public static float[] calcMaValues(ArrayList<PriceListing> argPriceHist, int argMaPeriod) {
		
		float[] maValues = new float[argPriceHist.size()];
		
		// Make sure we have at least the number of price points + 1 in history as the long
		// moving average in order to be able to calculate a valid MA value for both today
		// and yesterday for the selected range.
		if (argPriceHist.size() > argMaPeriod + 1) {
			
			// Start at the long MA Period position in the price history.
			int idx = argMaPeriod;

			while (idx < argPriceHist.size()) {
					
				// Calculate the short period MA value at the current position. Also calculate the short MA period value 
				// the previous day to be able to calculate if there has been a crossover.
				int sumShortMaPosition = idx;
				float sumCurrShortMaPeriod = 0.0f;

				while (sumShortMaPosition > (idx  - argMaPeriod) ) {
					sumCurrShortMaPeriod += argPriceHist.get(sumShortMaPosition).getClosingPrice();
					sumShortMaPosition--;
				}
				
				maValues[idx] = sumCurrShortMaPeriod / (float)argMaPeriod;

				idx++;
			}
		}
		
		return maValues;
	} // end calcMaValues()
	
	
	/**
	 * Checks if today is a bank holiday or something similar resulting in the exchange being 
	 * closed.
	 * 
	 * @param exchangeClosedDays A List containing bank holidays
	 * @param currentDate The Date of today
	 * @return a boolean indicating whether the exchange is closed or not.
	 */
	public static boolean checkExchangeClosed(List<String> exchangeClosedDays, Date currentDate) {
		// Check for days when the exchange is closed. If it is today and yesterdays
		// price history has been downloaded, do not download anything.
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date closedDate = null;
		boolean closedDay = false;
		
		for (String currentClosedDay : exchangeClosedDays) {
			
			String[] splitInfo = currentClosedDay.split(" ");
			
			try {
				closedDate = dateFormat.parse(splitInfo[0]);
			} 
			catch (ParseException e) {
				e.printStackTrace();
			}
			if ( (currentDate.getTime() - closedDate.getTime()) / (1000 * 60 * 60 * 24) == 0) {
				if (splitInfo[1].equalsIgnoreCase("hel")) {
					closedDay = true;
				}
			}
		}
		return closedDay;
	} // end checkExchangeClosed()
	
} // end class StockHandler
