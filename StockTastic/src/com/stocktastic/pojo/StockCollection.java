package com.stocktastic.pojo;

import java.util.List;

/**
 * Class that represents a list of stocks. It may be an index or
 * a user defined list of stocks.
 * 
 * @author stationaradm
 */
public class StockCollection {
	
	private String collectionName;
	
	private List<String> stockCollection;
	
	
	/**
	 * Constructor for class StockCollection
	 */
	public StockCollection(String collectionName, List<String> stockCollection) {
		this.collectionName = collectionName;
		this.stockCollection = stockCollection;
	} // end constructor StockCollection()


	public String getCollectionName() {
		return collectionName;
	}


	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}


	public List<String> getStockCollection() {
		return stockCollection;
	}


	public void setStockCollection(List<String> stockCollection) {
		this.stockCollection = stockCollection;
	}
} // end class StockCollection
