package com.stocktastic.pojo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class that contains price information for a specific date for a
 * specific stock.
 * 
 * @author stationaradm
 */
public class PriceListing implements Comparable<PriceListing> {

	private Date date;
	// Determines the opening price during this date.
	private float openingPrice;
	// Determines the highest listed price during this date.
	private float highestPrice;
	// Determines the highest lowest price during this date.
	private float lowestPrice;
	// Determines the listed closing price during this date.
	private float closingPrice;
	// Holds the listed adjusted closing price during a specific date.
	private float adjClosingPrice;
	// Determines the total volume this stock was traded this date.
	private int volume;
	
	public PriceListing(Date argDate, float argOpeningPrice, 
						float argHighestPrice, float argLowestPrice, 
						float argClosingPrice, float argAdjClosingPrice, int argVolume) {
		date = argDate;
		openingPrice = argOpeningPrice;
		highestPrice = argHighestPrice;
		lowestPrice = argLowestPrice;
		closingPrice = argClosingPrice;
		adjClosingPrice = argAdjClosingPrice;
		volume = argVolume;
	} // end constructor PriceListing()

	public Date getDate() {
		return date;
	}

	public String getShortDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Get the ratio for the closing price and the adjusted closing
	 * price. In this way, the adjusted price can be calculated according
	 * to the following formula:
	 * 
	 * adjusted price = unadjusted price * (adjusted close / unadjusted close)
	 * e.g. adjusted highest price = unadjusted highest price * (adjusted close / unadjusted close)
	 * 
	 * @return a float that is the ratio
	 */
	private float adjustedCloseRatio() {
		return adjClosingPrice / closingPrice;
	} // end adjustedCloseRatio()
	
	public float getOpeningPrice() {
		return openingPrice * adjustedCloseRatio();
	}

	public void setOpeningPrice(float openingPrice) {
		this.openingPrice = openingPrice;
	}

	public float getHighestPrice() {
		return highestPrice * adjustedCloseRatio();
	}

	public void setHighestPrice(float highestPrice) {
		this.highestPrice = highestPrice;
	}

	public float getLowestPrice() {
		return lowestPrice * adjustedCloseRatio();
	}

	public void setLowestPrice(float lowestPrice) {
		this.lowestPrice = lowestPrice;
	}

	public float getClosingPrice() {
		return closingPrice * adjustedCloseRatio();
	}

	public void setClosingPrice(float closingPrice) {
		this.closingPrice = closingPrice;
	}
	
	public float getAdjClosingPrice() {
		return adjClosingPrice;
	}

	public void setAdjClosingPrice(float adjClosingPrice) {
		this.adjClosingPrice = adjClosingPrice;
	}


	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "Date: " + new SimpleDateFormat("yyyy-MM-dd").format(date) +
				", Opening price; " + openingPrice + ", Highest Price: " + highestPrice +
				", Lowest Price: " + lowestPrice + ", Closing Price: " + closingPrice +
				", Adjusted Closing Price: " + adjClosingPrice + ", Volume: " + volume;
	} // end toString()

	@Override
	public int compareTo(PriceListing anotherPriceListing) {
		return this.getDate().compareTo(anotherPriceListing.getDate());
	}	
	
} // end class PriceListing
