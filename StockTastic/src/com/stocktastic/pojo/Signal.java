package com.stocktastic.pojo;

import java.util.Date;

/**
 * Class that represents a generated signal from a filter.
 * 
 * @author stationaradm
 */
public class Signal {

	// The name of the filter that generated the signal
	private String filterName;
	
	// The date the signal was generated
	private Date date;
	
	// A string that is the description of the signal
	private String description;

	public static enum SignalType { BUY, SELL}; 
	
	private SignalType buySellType;
	
	/**
	 * Constructor for class Stock with only the name
	 * of the stock as a parameter.
	 * 
	 * @param argName a String that is the name of the stock
	 */
	public Signal(String argFilterName, Date argDate, String argDescription, SignalType argBuySellType) {
		filterName = argFilterName;
		date = argDate;
		description = argDescription;
		buySellType = argBuySellType;
	}
	
	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;			
		}
		if ( !(obj instanceof Signal) ) {
			return false;
		}
		Signal otherSignal = (Signal) obj;
		return (date.before( otherSignal.getDate()) && 
				filterName.equals(otherSignal.getFilterName()));
	} // end equals()

	public SignalType getBuySellType() {
		return buySellType;
	}

	public void setBuySellType(SignalType argBbuySellType) {
		this.buySellType = argBbuySellType;
	}
	
} // end class Stock
