package com.stocktastic.pojo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Class that represents a stock index.
 * 
 * @author stationaradm
 */
public class Index {

	// Full name of the index, eg Stockholm OMX S30
	private String indexName;
	
	// Stock symbol in the index (e.g. OMXS30)
	private String symbol;
	
	// An array of the components of the index (e.g. Nordea A, Swedbank A etc)
	private String[] components;
	
	// A list of historic prices.
	private ArrayList<PriceListing> prices;
	
	/**
	 * Constructor for class Stock with only the name
	 * of the stock as a parameter.
	 * 
	 * @param argName a String that is the name of the stock
	 */
	public Index(String argIndexName, String argSymbol) {
		indexName = argIndexName;
		symbol = argSymbol;
	}
	
	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String argIndexName) {
		indexName = argIndexName;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String argSymbol) {
		symbol = argSymbol;
	}

	public ArrayList<PriceListing> getPrices() {
		return prices;
	}

	public void setPrices(ArrayList<PriceListing> prices) {
		this.prices = prices;
	}
	
	
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;			
		}
		if ( !(obj instanceof Index) ) {
			return false;
		}
		Index otherIndex = (Index) obj;
		return (indexName.equals( otherIndex.getIndexName()) &&
				symbol.equals(otherIndex.getSymbol()) );
	} // end equals()

	/**
	 * Returns the latest available Date of the historic index prices. 
	 * @return a Date that is the latest available date for the index prices
	 */
	public Date getLatestDownloadedDate()  {

		Date lastestDownload = null;
		
		if (prices != null && !prices.isEmpty()) {
			lastestDownload =  prices.get(prices.size() -1).getDate();
		}
		
		return lastestDownload;
	} // end getLatestDownloadedDate()
	
	
	public void printPrices() {
		System.out.printf("Price history for: %s%n", indexName);
		System.out.printf("Total number of recorded dates: %d%n", prices.size());
		for (PriceListing currentPrice : prices) {
			System.out.println(currentPrice.toString());
		}
	} // end printPrices()
	
} // end class Stock
