package com.stocktastic.pojo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Class that represents a listed stock.
 * 
 * @author stationaradm
 */
public class Stock {

	// Full name of a stock (e.g. Tieto Enator)
	private String companyName;
	
	// Stock symbol in the index (e.g. TIEN.ST)
	private String symbol;
	
	// An array of the indexes (e.g. OMXS30, S&P500 or Aktietorget)
	private String[] index;
	
	// A list of historic prices.
	private ArrayList<PriceListing> prices;
	
	// A list of buy/sell signals for this stock.
	private ArrayList<Signal> signals;
	
	/**
	 * Constructor for class Stock with only the name
	 * of the stock as a parameter.
	 * 
	 * @param argName a String that is the name of the stock
	 */
	public Stock(String argCompanyName, String argSymbol) {
		companyName = argCompanyName;
		symbol = argSymbol;
		signals = new ArrayList<Signal>();
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public ArrayList<PriceListing> getPrices() {
		return prices;
	}

	public void setPrices(ArrayList<PriceListing> prices) {
		this.prices = prices;
	}
	
	
	public ArrayList<Signal> getSignals() {
		return signals;
	}

	public void setSignals(ArrayList<Signal> signals) {
		this.signals = signals;
	}

	public void addSignal(Signal argSignal) {
		signals.add(argSignal);
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;			
		}
		if ( !(obj instanceof Stock) ) {
			return false;
		}
		Stock otherStock = (Stock) obj;
		return (companyName.equals( otherStock.getCompanyName()) &&
				symbol.equals(otherStock.getSymbol()) );
	} // end equals()

	/**
	 * Returns the latest available Date of the historic stock prices. 
	 * @return a Date that is the latest available date for the stock prices
	 */
	public Date getLatestDownloadedDate()  {

		Date lastestDownload = null;
		
		if (prices != null && !prices.isEmpty()) {
			lastestDownload =  prices.get(prices.size() -1).getDate();
		}
		
		return lastestDownload;
	} // end getLatestDownloadedDate()
	
	
	public void printPrices() {
		System.out.printf("Price history for: %s%n", companyName);
		System.out.printf("Total number of recorded dates: %d%n", prices.size());
		for (PriceListing currentPrice : prices) {
			System.out.println(currentPrice.toString());
		}
	} // end printPrices()
	
} // end class Stock
