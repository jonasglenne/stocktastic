package com.stocktastic;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// TODO Create an interface that abstracts the implementation (e.g. downloading prices from Yahoo) to be defensive against changing APIs etc.

/**
 * Class that handles downloads of historic stock prices. Contains a thread
 * pool for concurrent downloads.
 * 
 * @author stationaradm
 */
public class DownloadHandler {
	
	private static final Logger log4j = LogManager.getLogger(DownloadHandler.class.getName());

	public static enum DownloadType {HISTORIC, QUOTE, INDEX};

	// The base url to yahoo finance api for historic prices and index. Combine with stock short name 
	// and dates for complete url
	public static String historyBaseUrl = "http://ichart.yahoo.com/table.csv?";

	// The base url to yahoo finance api for historic prices and index. Combine with stock short name 
	// and dates for complete url. Separate url if better alternative that can show e.g. OMXSPI or OMXSGI is found
	public static String historicIndexBaseUrl = historyBaseUrl;

	// The base url to yahoo finance api for stock quote. Combine with stock short name 
	// and dates for complete url. Note that this url has to be used with today's stock prices
	// TODO: Find out when the prices are available from historic url.
	public static String quoteBaseUrl = "http://download.finance.yahoo.com/d/quotes.csv?";

	// The executor for this handler. Will be of type FixedThreadPool.
	private ExecutorService execService;
	
	
	/**
	 * Constructor for class DownloadHandler.
	 */
	public DownloadHandler() {
		execService = Executors.newFixedThreadPool(5);
	} // end constructor DownloadHandler()
	

	/**
	 * Downloads historic prices for an index within the given range.
	 * 
	 * @param indexName a String that is the name of the index to be downloaded
	 * @param startDate a Date that is the start of the requested date range to download prices within
	 * @param endDate a Date that is the end of the requested date range to download prices within
	 */
	public void downloadIndexHistory(String indexName, Calendar startDate, Calendar endDate) {
		// http://www.euroinvestor.se/stock/historicalquotes.aspx?instrumentId=2085370&format=CSV
		// alt https://www.netfonds.se/quotes/paperhistory.php?paper=OMXS30.ST&csv_format=csv
		// http://www.nasdaqomxnordic.com/indexes/historical_prices?Instrument=SE0000337842&InstrumentName=OMX%20Stockholm%2030%20Index
		// http:///ichart.yahoo.com/table.csv?s=%5EOMX&a=6&b=9&c=1991&d=7&e=4&f=2014&g=d&ignore=.csv
		String fullUrl = historicIndexBaseUrl;
		fullUrl += "s=" + indexName;
		fullUrl += "&a=" + startDate.get(Calendar.MONTH);
		fullUrl += "&b=" + startDate.get(Calendar.DAY_OF_MONTH);
		fullUrl += "&c=" + startDate.get(Calendar.YEAR);
		fullUrl += "&d=" + endDate.get(Calendar.MONTH);
		fullUrl += "&e=" + endDate.get(Calendar.DAY_OF_MONTH);
		fullUrl += "&f=" + endDate.get(Calendar.YEAR);
		fullUrl += "&g=d";
		fullUrl += "&ignore=.csv";
		log4j.debug("Download index historic url: {}", fullUrl);
		
		execService.execute(new DownloadTask(fullUrl, null, indexName, DownloadType.INDEX));
	} // end downloadIndex()
	

	/**
	 * Downloads historic prices for an index within the given range and also download price quote
	 * for today.
	 * 
	 * @param indexName a String that is the name of the index to be downloaded
	 * @param startDate a Date that is the start of the requested date range to download prices within
	 * @param endDate a Date that is the end of the requested date range to download prices within
	 */
	public void downloadIndexHistoryAndQuote(String indexName, Calendar startDate, Calendar endDate) {
		// http://www.euroinvestor.se/stock/historicalquotes.aspx?instrumentId=2085370&format=CSV
		// alt https://www.netfonds.se/quotes/paperhistory.php?paper=OMXS30.ST&csv_format=csv
		// http://www.nasdaqomxnordic.com/indexes/historical_prices?Instrument=SE0000337842&InstrumentName=OMX%20Stockholm%2030%20Index
		// http:///ichart.yahoo.com/table.csv?s=%5EOMX&a=6&b=9&c=1991&d=7&e=4&f=2014&g=d&ignore=.csv
		String fullUrl = historicIndexBaseUrl;
		fullUrl += "s=" + indexName;
		fullUrl += "&a=" + startDate.get(Calendar.MONTH);
		fullUrl += "&b=" + startDate.get(Calendar.DAY_OF_MONTH);
		fullUrl += "&c=" + startDate.get(Calendar.YEAR);
		fullUrl += "&d=" + endDate.get(Calendar.MONTH);
		fullUrl += "&e=" + endDate.get(Calendar.DAY_OF_MONTH);
		fullUrl += "&f=" + endDate.get(Calendar.YEAR);
		fullUrl += "&g=d";
		fullUrl += "&ignore=.csv";
		log4j.debug("Download index historic url: {}", fullUrl);
		
		String quoteUrl = quoteBaseUrl;
		quoteUrl += "s=" + indexName;
		quoteUrl += "&f=d1o0h0g0l1v0";
		quoteUrl += "&e=.csv";
		log4j.debug("Download index quote url: {}", quoteUrl);
		
		execService.execute(new DownloadTask(fullUrl, quoteUrl, indexName, DownloadType.INDEX));
	} // end downloadIndices()

	/**
	 * Downloads the historic stock prices for one specific stock with the given start and end date.
	 * 
	 * @param stockName a String that holds the ticker name for the stock to be downloaded
	 * @param startDate a Calendar representing the latest available price history
	 * @param endDate a Calendar representing the date of today.
	 */
	public void downloadStockPriceHistory(String stockName, Calendar startDate, Calendar endDate) {
		
		String historyUrl = historyBaseUrl;
		historyUrl += "s=" + stockName;
		historyUrl += "&a=" + startDate.get(Calendar.MONTH);
		historyUrl += "&b=" + startDate.get(Calendar.DAY_OF_MONTH);
		historyUrl += "&c=" + startDate.get(Calendar.YEAR);
		historyUrl += "&d=" + endDate.get(Calendar.MONTH);
		historyUrl += "&e=" + endDate.get(Calendar.DAY_OF_MONTH);
		historyUrl += "&f=" + endDate.get(Calendar.YEAR);
		historyUrl += "&g=d";
		historyUrl += "&ignore=.csv";
		log4j.debug("Download stock historic url: {}", historyUrl);
		
		execService.execute(new DownloadTask(historyUrl, null, stockName, DownloadType.HISTORIC));
	} // end downloadStockPriceHistory

	/**
	 * Downloads the historic stock prices for one specific stock with the given start and end date.
	 * 
	 * @param stockName a String that holds the ticker name for the stock to be downloaded
	 * @param startDate a Calendar representing the latest available price history
	 * @param endDate a Calendar representing the date of today.
	 */
	public void downloadStockPriceHistoryAndQuote(String stockName, Calendar startDate, Calendar endDate) {
		
		String historyUrl = historyBaseUrl;
		historyUrl += "s=" + stockName;
		historyUrl += "&a=" + startDate.get(Calendar.MONTH);
		historyUrl += "&b=" + startDate.get(Calendar.DAY_OF_MONTH);
		historyUrl += "&c=" + startDate.get(Calendar.YEAR);
		historyUrl += "&d=" + endDate.get(Calendar.MONTH);
		historyUrl += "&e=" + endDate.get(Calendar.DAY_OF_MONTH);
		historyUrl += "&f=" + endDate.get(Calendar.YEAR);
		historyUrl += "&g=d";
		historyUrl += "&ignore=.csv";
		log4j.debug("Download stock historic url: {}", historyUrl);
		
		String quoteUrl = quoteBaseUrl;
		quoteUrl += "s=" + stockName;
		quoteUrl += "&f=d1o0h0g0l1v0";
		quoteUrl += "&e=.csv";
		log4j.debug("Download stock quote url: {}", quoteUrl);
		
		execService.execute(new DownloadTask(historyUrl, quoteUrl, stockName, DownloadType.HISTORIC));
	} // end downloadStockPriceHistoryAndQuote
	
	/**
	 * Downloads the stock prices for one specific stock if the date for the price info is today.
	 * 
	 * @param stockName a String that holds the ticker name for the stock to be downloaded
	 */
	public void downloadStockPriceQuote(String stockName) {
		String quoteUrl = quoteBaseUrl;
		quoteUrl += "s=" + stockName;
		quoteUrl += "&f=d1o0h0g0l1v0";
		quoteUrl += "&e=.csv";
		log4j.debug("Download stock quote url: {}", quoteUrl);
		execService.execute(new DownloadTask(null, quoteUrl, stockName, DownloadType.QUOTE));
	} // end downloadStockPriceQuote()
	
	/**
	 * Downloads the index prices for one specific index if the date for the price info is today.
	 * 
	 * @param indexName a String that holds the ticker name for the stock to be downloaded
	 */
	public void downloadIndexPriceQuote(String argIndexName) {
		String quoteUrl = quoteBaseUrl;
		quoteUrl += "s=" + argIndexName;
		quoteUrl += "&f=d1o0h0g0l1v0";
		quoteUrl += "&e=.csv";
		log4j.debug("Download index quote url: {}", quoteUrl);
	
		execService.execute(new DownloadTask(null, quoteUrl, argIndexName, DownloadType.QUOTE));
	} // end downloadIndexPriceQuote()
	
	/**
	 * Downloads the historic stock prices for the stocks in a list with the given start and end date.
	 * 
	 * @param stockNames a List that contains the ticker names for the stocks to be downloaded
	 * @param startDate a Calendar representing the latest available price history
	 * @param endDate a Calendar representing the date of today.
	 */
	public void downloadStockPriceHistory(List<String> stockNames, Calendar startDate, Calendar endDate) {
		
		for (String currentStock : stockNames) {
			
			String historyUrl = historyBaseUrl;
			historyUrl += "s=" + currentStock;
			historyUrl += "&a=" + startDate.get(Calendar.MONTH);
			historyUrl += "&b=" + startDate.get(Calendar.DAY_OF_MONTH);
			historyUrl += "&c=" + startDate.get(Calendar.YEAR);
			historyUrl += "&d=" + endDate.get(Calendar.MONTH);
			historyUrl += "&e=" + endDate.get(Calendar.DAY_OF_MONTH);
			historyUrl += "&f=" + endDate.get(Calendar.YEAR);
			historyUrl += "&g=d";
			historyUrl += "&ignore=.csv";
			log4j.debug("Download stock history url: {}", historyUrl);

			String quoteUrl = quoteBaseUrl;
			quoteUrl += "s=" + stockNames;
			quoteUrl += "&f=d1o0h0g0l1v0";
			quoteUrl += "&e=.csv";
			log4j.debug("Download stock quote url: {}", quoteUrl);

			execService.execute(new DownloadTask(historyUrl, quoteUrl, currentStock, DownloadType.HISTORIC));
		}
	} // end downloadPriceHistory
	
	
	/**
	 * Shuts down the threadpool of this handler and wait with further execution
	 * until all tasks are done and the threadexecutor has shut down.
	 */
	public void shutDownHandlerAndWait() {
		execService.shutdown();

		try {
			execService.awaitTermination(5, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	} // end shutDownHandler()
	
	
	/**
	 * A task that downloads the file at the given URL location via HTTP and
	 * writes the file to disk.
	 * 
	 * @author stationaradm
	 */
	private class DownloadTask implements Runnable {
		
		private URL downloadHistoryURL;
		private URL downloadQuoteURL;
		private HttpURLConnection conn;
		private String stockName;
		private DownloadType downloadType;
		
		/**
		 * Constructor for class DownloadTask
		 * 
		 * @param historyUrl a String that is the URL to the historic prices (up to but not including today's price) file to download
		 * @param quoteUrl a String that is the URL to the prices quote (today's price) file to download
		 * @param argStockName a String that is the name of the stock for which to download prices
		 * @param type a DownloadType enum that tells if this historic prices or today's quote
		 */
		public DownloadTask(String historyUrl, String quoteUrl, String argStockName, DownloadType type) {
			try {
				if (historyUrl != null) {
					downloadHistoryURL = new URL(historyUrl);
				}
				if (quoteUrl != null) {
					downloadQuoteURL = new URL(quoteUrl);
				}
			} 
			catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			stockName = argStockName; 
			
			downloadType = type;
		} // end constructor DownloadTask

		
		@Override
		public void run() {
			
			// Read the existing file if there is one.
			List<String> fileLines = new ArrayList<String>();
			BufferedReader bufferReader = null;
			File checkFile = new File(StockTastic.PRICEHISTORYLOCATION + stockName + ".csv");
			
			// Try reading the existing file in a List.
			try {
				if (checkFile.exists()) {
					bufferReader = new BufferedReader(new FileReader(checkFile));
					String line = null;
					while ((line = bufferReader.readLine()) != null) {
						if (!line.matches(".*Date.*")) {
								fileLines.add(line);
						}
					}
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				try {
					if (bufferReader != null) {
						bufferReader.close();
					}
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			OutputStreamWriter out = null;
			
			try {
				
				// Download historic prices
				if (downloadHistoryURL != null) {
					conn = (HttpURLConnection)downloadHistoryURL.openConnection();
					int respCode = conn.getResponseCode();
					
					if (respCode == 200) {
						
						conn.connect();
						BufferedReader buffReader  = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						
						// TODO Check that the first price line makes sense. If yahoo is broken in some way, do not update the file.
						String line = null;
						while ((line = buffReader.readLine()) != null) {

							if (!line.matches(".*Date.*")) {
								fileLines.add(line);
							}
						}

						Collections.sort(fileLines);
					}
					else {
						log4j.error("Could not find prices for: {}", downloadQuoteURL);
					}
				}
				
				// Download price quote for today
				if (downloadQuoteURL != null) {
					conn = (HttpURLConnection)downloadQuoteURL.openConnection();
					int respCode = conn.getResponseCode();
					
					if (respCode == 200) {
						
						conn.connect();
						BufferedReader buffReader  = new BufferedReader(new InputStreamReader(conn.getInputStream()));
							
						String todaysQuote = buffReader.readLine();
						todaysQuote = todaysQuote.replace("\"", "");
						String[] quoteArray = todaysQuote.split(",");
						// Yahoo gives date in this format 7/18/2014, change to YYYY-MM-DD
						SimpleDateFormat fromProvider = new SimpleDateFormat("MM/dd/yyyy");
						SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
						String reformattedDate = null;
						
						try {
						    reformattedDate = myFormat.format(fromProvider.parse(quoteArray[0]));
						} 
						catch (ParseException e) {
							log4j.error("Error for: {}", downloadQuoteURL);
						    e.printStackTrace();
						}
						// Note that the last position is corrected close for historic price but since
						// the day is not over yet it is not possible to give this info.
						// TODO Check if the corrected closed can be dashed.
						String preparedLine = reformattedDate + "," + quoteArray[1] + "," + quoteArray[2] + "," 
								       + quoteArray[3] + "," + quoteArray[4] + "," + quoteArray[5] + "," 
								       + quoteArray[4];
						
						// Remove the previous latest quote from the history file if it contains today's date, 
						// i.e. we are replacing the latest quote.
						int linesSize = fileLines.size();
						if (fileLines.get(linesSize -1).contains(reformattedDate)) {
							fileLines.remove(linesSize- 1);
							fileLines.add(preparedLine);
						}
						else if (fileLines.get(linesSize -2).contains(reformattedDate)) {
							fileLines.remove(linesSize - 1);
							fileLines.remove(linesSize - 2);
							fileLines.add(preparedLine);
						}
						else {
							// Add the updated quote from the history file.
							fileLines.add(preparedLine);
						}
					}
					else {
						log4j.error("Could not find prices for: {}", downloadQuoteURL);
					}
						
					Collections.sort(fileLines);
				}

				// Print the results to file.
				out = new FileWriter(checkFile);
				for (String fileLine : fileLines) {
					if (!fileLine.matches(".*Date.*")) {
						out.write(fileLine + "\n");
					}
				}
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				try {
					if (out != null) {
						out.close();
					}
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
				conn.disconnect();
				conn = null;
			}
		} // end run()	
	} // end class DownloadTask
	
} // end class DownloadHandler
