package com.stocktastic.filter;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.filter.StockFilter.Perspective;
import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Stock;

/**
 * Calculates the current trend for the given stock. Trend can be 
 * up, down or sideways. 
 * 
 * To calculate the trend the two highest prices and the two lowest 
 * prices are calculated. If the first price of the two for both highest 
 * and lowest are below the second 
 * are below the 
 * and basic functionality. Override applyFilter() to define behavior
 * 
 * @author stationaradm
 */
public class TrendCalculator {
	
	private static Logger log4j = LogManager.getLogger(TrendCalculator.class.getName());

	/*
	* Defines a trend for a stocks data.
	* 
	* ERROR - A Trend could not be determined due to errors
	* DOWN - The trend for the stock date is down
	* UP - The trend for the stock data is up
	* SIDEWAYS - The stock data trend is not up, neither down.
	*/
	public static enum Trend { ERROR, DOWN, SIDEWAYS, UP}; 
	
	//TODO Work more on the advanced trend checking.
	/**
	 * Calculates the trend for the selected stock with the given perspective and from
	 * the selected position in history.
	 * 
	 * @param stock the selected stock
	 * @param trendPerspective the Perspective chosen, short, medium or long term.
	 * @param signalPosition the position in the history where a signal was given in number of days offset from today.
	 * 
	 * @return an Enum indicating the trend of the stock with the given perspective.
	 */
	public static Trend advancedTrend(Stock stock, Perspective trendPerspective, int signalPosition) {
		Stock selectedStock = stock;
		Trend trend = null;
		
		ArrayList<PriceListing> priceHist = selectedStock.getPrices();
		
		// Start the trend search from the day the signal was given minus 
		// the trend perspective (e.g 90 days before signal in medium perspective)
		// If there is not enough days (which would result in a array index out of bounds error, 
		// return an error informing that there is not enough days in the price history to calculate
		// the trend from the day the signal was given with the chosen perspective

		int position = signalPosition - trendPerspective.getValue();

		if (signalPosition - trendPerspective.getValue() < 0) {
			System.out.format("Not enough days in history to calculate trend before this " +
					          "date at chosen perspective, signal date: %s%n", 
					          priceHist.get(signalPosition).getDate());
			return Trend.ERROR;
		}
		
		// Starting point is the highest and lowest price at the start of the perspective (e.g 90 days back 
		// for medium trend from the given signal.
		float currentClosingPrice = priceHist.get(position).getClosingPrice();
		System.out.format("Starting closing prices is: %f%n", currentClosingPrice);

		// Collect all highs and lows during the chosen perspective (short, medium, long) up to the 
		// day of the signal.
		ArrayList<PriceListing> highs = new ArrayList<PriceListing>();
		ArrayList<PriceListing> lows = new ArrayList<PriceListing>();

		// Only add to highs if we are currently in a rising trend.
		// At start assume both directions are possible, therefore
		// setting both to true.
		boolean priceRising = true;
		boolean priceFalling = true;
		
		// Loop through the trend length to the position where the event was found adding
		// highs and lows in two different arrays as the price turns.
		while (position < signalPosition) {
			
			position++;
			
			//System.out.format("Current closing price: %f, %n", currentClosingPrice);
			//System.out.format("Comparing to: %f, %n", priceHist.get(position).getClosingPrice());
			
			// IF:      If next price is lower than the current one and we are in a rising trend, this is a high peak, add to highs
			// ELSE IF: If next price is higher than the current one and we are in a sinking trend, this is a low mark, add to lows
			// ELSE:    Next price is neither higher or lower than the current one but it is according to trend, 
			//          therefore do not add it to highs or lows
			if (currentClosingPrice > priceHist.get(position).getClosingPrice() && priceRising == true) {
				highs.add(priceHist.get(position -1));
				//System.out.format("Adding closing price to highs: %f%n", currentClosingPrice);
				priceRising = false;
				priceFalling = true;
			}
			else if (currentClosingPrice < priceHist.get(position).getClosingPrice() && priceFalling == true) {
				lows.add(priceHist.get(position -1));
				//System.out.format("Adding closing price to lows: %f%n", currentClosingPrice);
				priceFalling = false;
				priceRising = true;
			}
			else {
				//System.out.format("Did not make it: %f%n", currentClosingPrice);
			}
			
			currentClosingPrice = priceHist.get(position).getClosingPrice();
		}
		
		if (highs.size() > 1 && lows.size() > 1) {
			
			//System.out.format("Enough data points to check trend, highs: %d, lows: %d%n", highs.size(), lows.size());
			
			//System.out.format("Checking highs%n");
			for (PriceListing price : highs) {
				//System.out.format("Date: %s, price: %f%n", price.getDate(), price.getClosingPrice());
			}

			//System.out.format("Checking lows%n");
			for (PriceListing price : lows) {
				//System.out.format("Date: %s, price: %f%n", price.getDate(), price.getClosingPrice());
			}

			float peakSlope = highs.get(0).getHighestPrice() / highs.get(highs.size() -1).getHighestPrice();
			boolean higher = true;
			//System.out.format("Checking highs%n");
	
			for (int index = 0; index < highs.size() - 1 && higher == true; index++) {
				
				//System.out.format("Price at position %d: %f%n", index, highs.get(index).getHighestPrice());
				
				if (highs.get(index).getHighestPrice() > highs.get(index + 1).getHighestPrice()) {
					higher = false;
					//System.out.format("Highs broken at position %d: %f%n", index + 1, highs.get(index + 1).getHighestPrice());
				}
			}
	
			float fallSlope = lows.get(0).getLowestPrice() / lows.get(lows.size() -1).getLowestPrice();
			boolean lower = true;
			//System.out.format("Checking lows%n");
	
			for (int index = 0; index < lows.size() -1 && lower == true; index++) {
				//System.out.format("Price at position %d: %f%n", index, lows.get(index).getHighestPrice());
	
				if (lows.get(index).getLowestPrice() > lows.get(index + 1).getLowestPrice()) {
					lower = false;
					//System.out.format("Lows broken at position %d: %f%n", index + 1, lows.get(index + 1).getLowestPrice());
				}
			}
			//System.out.format("higher: %b%n", higher);
			//System.out.format("lower: %b%n", lower);
		}
		else {
			//System.out.format("Not enough data points to check trend, highs: %d, lows: %d%n", highs.size(), lows.size());
		}

		return trend;
	} // end advancedTrend
	
	/**
	 * A simple trend function. Takes the first price item at signal position - trend length (e.g. 2010-01-31 - 30 days)
	 * and checks whether that price is lower or higher than the data at the signal position (e.g. 2010-01-31).
	 * 
	 * @param stock a Stock that is the stock to calculate trend
	 * @param trendPerspective a Perspective that determines the length of the trend check
	 * @param signalPosition an int that defines which index in the price history of the selected stock where the signal was given.
	 * @return a Trend that gives the trend for the selected signal in the selected perspective
	 */
	public static Trend simpleTrend(Stock stock, Perspective trendPerspective, int signalPosition) {
		Stock selectedStock = stock;
		Trend trend = null;
		
		ArrayList<PriceListing> priceHist = selectedStock.getPrices();
		
		// Start the trend search from the day the signal was given minus 
		// the trend perspective (e.g 90 days before signal in medium perspective)
		// If there is not enough days (which would result in a array index out of bounds error, 
		// return an error informing that there is not enough days in the price history to calculate
		// the trend from the day the signal was given with the chosen perspective

		int position = signalPosition - trendPerspective.getValue();

		if (signalPosition - trendPerspective.getValue() < 0) {
			log4j.trace("Not enough days in history to calculate trend for stock: {} before this date at " + 
						"chosen perspective, signal date: {}", 
						selectedStock.getCompanyName(), priceHist.get(signalPosition).getDate());
			return Trend.ERROR;
		}
		
		float firstClose = priceHist.get(position).getClosingPrice();
		float lastClose = priceHist.get(signalPosition).getClosingPrice();
		
		if ( (firstClose / lastClose) > 1.02) {
			trend = Trend.DOWN; 
		}
		else if ((firstClose / lastClose) < 0.98) {
			trend = Trend.UP;
		}
		else {
			trend = Trend.SIDEWAYS;
		}
		
		return trend;
	} // end simpleTrend

	/**
	 * Calculates a trend for the ma values.
	 * 
	 * @param argStock a Stock that is the stock for which the MA value trend is calculated
	 * @param argMaValues an array of floats that are the ma values for the price history of a stock
	 * @param trendPerspective a Perspective that is the length of the perspective (e.g. SHORT, MEDIUM, LONG)
	 * @param signalPosition the position in the price history where we start to calculate the trend
	 * @param argMaPeriod an int that determines the length of the MA periods in days
	 * 
	 * @return a Trend indicating if the trend is UP, DOWN or SIDEWAYS
	 */
	public static Trend calcMaTrend(Stock argStock, float[] argMaValues, Perspective trendPerspective, int signalPosition, int argMaPeriod) {
		
		ArrayList<PriceListing> prices = argStock.getPrices();
		Trend maTrend = null;
		float[] maValues = argMaValues;

		if (signalPosition - trendPerspective.getValue() < 0) {
			log4j.trace("Not enough days in history to calculate MA trend for {} at chosen perspective atsignal date: {}", 
					    argStock.getCompanyName(), prices.get(signalPosition).getShortDateString());
			return Trend.ERROR;
		}
		
		float currentMaValue = maValues[signalPosition];
		float oldMaValue = maValues[signalPosition - trendPerspective.getValue()];

		if ((currentMaValue / oldMaValue) > 1.02) {
			maTrend = Trend.UP; 
			log4j.debug("Trend is UP for MA value: {} at signal date: {}", argMaPeriod, prices.get(signalPosition).getShortDateString());
		}
		else if ((currentMaValue / oldMaValue) < 0.98) {
			log4j.debug("Trend is DOWN for MA value: {} at signal date: {}", argMaPeriod, prices.get(signalPosition).getShortDateString());
			maTrend = Trend.DOWN;
		}
		else {
			log4j.debug("Trend is SIDEWAYS for MA value: {} at signal date: {}", argMaPeriod, prices.get(signalPosition).getShortDateString());
			maTrend = Trend.SIDEWAYS;
		}

		return maTrend;
	} // end calcMaValues()

} // end class StockFilter
