package com.stocktastic.filter;

import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;

/**
 * Abstract class that represents a filter. Contains the filter name
 * and basic functionality. Override applyFilter() to define behavior
 * 
 * @author stationaradm
 */
public class MADoubleCrossOverFilter extends StockFilter {
	
	private static Logger log4j;

	// Represents the number of days to calculate the short moving average value
	private int shortMaPeriod;
	
	// Represents the number of days to calculate the long moving average value
	private int longMaPeriod;
	
	// Short MA values for the price history
	private float[] shortMaValues;
	
	// Long MA values for the price history
	private float[] longMaValues;

	/**
	 * Constructor for class StockFilter with only the name
	 * of the filter as a parameter.
	 * 
	 * @param shortMa an Integer that represents the number of days to calculate the moving average value
	 * @param longMaPeriod an Integer that represents the number of days to calculate the moving average value
	 */
	public MADoubleCrossOverFilter(int argShortMaPeriod, int argLongMaPeriod) {
		super("MA Double Crossover", "Generates buy signal when short MA period crosses long MA period from below and sell if opposite.");
		shortMaPeriod = argShortMaPeriod;
		longMaPeriod = argLongMaPeriod;
		
		// Since the filter name is variable with the periods, update the filter name (it is used for comparison in Signal objects).
		setFilterName("MA Double Crossover " + shortMaPeriod + "/" + longMaPeriod);
		
		log4j = LogManager.getLogger(MADoubleCrossOverFilter.class.getName());
	} // end constructor MAFilter

	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		
		Date currentDate = StockTasticUtils.getCurrentDate();	
		stock = argStock;
		priceHist = stock.getPrices();
		boolean signalFound = false;
		
		shortMaValues = StockTasticUtils.calcMaValues(priceHist, shortMaPeriod);
		longMaValues = StockTasticUtils.calcMaValues(priceHist, longMaPeriod);
		
		// Make sure we have at least the number of price points + 1 in history as the long
		// moving average in order to be able to calculate a valid MA value for both today
		// and yesterday for the selected range.
		if (priceHist.size() > longMaPeriod + 1) {
			
			int idx = priceHist.size() -1;

			do {
					
				long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(idx).getDate());
				
				// If the current short MA value is above the long MA value and yesterday the short MA
				// value was below the long MA value, a double crossover has occured which is a BUY signal.
				if (shortMaValues[idx] > longMaValues[idx] && shortMaValues[idx -1] <= longMaValues[idx -1]) {

					log4j.info(differenceInDays + " day(s) old. MA" + shortMaPeriod + "/" + longMaPeriod + "double crossover BUY signal for " +
							stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString()); 
					log4j.trace("Current short MA: " + shortMaValues[idx] + ", previous short MA: " + shortMaValues[idx -1]); 
					log4j.trace("Current long MA: " + longMaValues[idx] + ", previous long MA: " + longMaValues[idx -1]);
					log4j.trace("Current closing price: " + priceHist.get(idx).getClosingPrice());
					
					stock.addSignal(new Signal(getFilterName(), 
									priceHist.get(idx).getDate(), 
									differenceInDays + " days old MA" + shortMaPeriod + "/"+ longMaPeriod + " double crossover BUY signal for " + 
									stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + "with short MA at: " + 
									shortMaValues[idx] + " and long MA at: " + longMaValues[idx],
									SignalType.BUY));
					
					signalFound = true;
				}
				
				// If the current short MA value is below the last long moving average and the previous short MA 
				// value was above the long MA value, it is a SELL signal
				if (shortMaValues[idx] < longMaValues[idx] && shortMaValues[idx -1] >= longMaValues[idx -1]) {

					log4j.info(differenceInDays + " day(s) old. MA" + shortMaPeriod + "/" + longMaPeriod + "double crossover SELL signal for " +
							stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString()); 
					log4j.trace("Current short MA: " + shortMaValues[idx] + ", previous short MA: " + shortMaValues[idx -1]); 
					log4j.trace("Current long MA: " + longMaValues[idx] + ", previous long MA: " + longMaValues[idx -1]);
					log4j.trace("Current closing price: " + priceHist.get(idx).getClosingPrice());
					
					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(idx).getDate(), 
							differenceInDays + " days old MA" + shortMaPeriod + "/"+ longMaPeriod + " double crossover SELL signal for " + 
							stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + "with short MA at: " + 
							shortMaValues[idx] + " and long MA at: " + longMaValues[idx],
							SignalType.SELL));
					
					signalFound = true;
				}

				log4j.trace("Current Short MA value was %f and price was: %f%n", shortMaValues[idx], 
						          priceHist.get(priceHist.size() -1).getClosingPrice());
				log4j.trace("Previous Short MA value was %f and price was: %f%n", shortMaValues[idx -1], 
						          priceHist.get(priceHist.size() -2).getClosingPrice());
				log4j.trace("Current Long MA value was %f and price was: %f%n", longMaValues[idx], 
						          priceHist.get(priceHist.size() -1).getClosingPrice());
				log4j.trace("Previous Long MA value was %f and price was: %f%n%n%n", longMaValues[idx -1], 
						          priceHist.get(priceHist.size() -2).getClosingPrice());
				log4j.trace("Counting date: %s%n", priceHist.get(idx).getShortDateString());

				idx--;
				
			} while (continueUntilSignalFound == true && idx > longMaPeriod && signalFound == false);
		}
		else {
			System.out.format("Not enough price history to calculate moving average for %s,  required: %d, found: %d%n", 
				      stock.getCompanyName(), longMaPeriod , (priceHist.size() -1));
		}
	} // end applyFilter()

	@Override
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		shortMaValues = StockTasticUtils.calcMaValues(priceHist, shortMaPeriod);
		longMaValues = StockTasticUtils.calcMaValues(priceHist, longMaPeriod);
		
		float byingPrice = 0.0f;
		float totalWinnings = 0.0f;
		
		// Make sure we have at least the selected long MA-period + 1 price points in history in order to be able to
		// calculate the MA value.
		if (priceHist.size() >= (longMaPeriod + 1) ) {
			
			// Start at the long MA Period position in the price history -1 because of array starting with index 0.
			int idx = longMaPeriod;
			
			if (argStartDate != null) {
				idx = checkStartDate(argStartDate);
			}

			if (idx > -1) {
				while (idx < priceHist.size()) {
					
					// If the previous short moving average at the position was below the previous 
					// last long moving average and the short moving average at the position crossed 
					// the long moving average at the position, it is a buy signal
					if (shortMaValues[idx -1] <= longMaValues[idx -1] && shortMaValues[idx] > longMaValues[idx] && byingPrice == 0.0f) {
						
						log4j.debug("MA" + shortMaPeriod + "/" + longMaPeriod + "/" + longMaPeriod + " short MA crossover long MA BUY signal for " + 
					            	stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + " " + 
					            	" with price: " + priceHist.get(idx).getClosingPrice()); 
						log4j.trace("prevShortMaValue < prevLongMaValue : " + shortMaValues[idx -1] + " < " + longMaValues[idx -1]);
						log4j.trace("currShortMaValue > currLongMaValue : " + shortMaValues[idx] + " > " + longMaValues[idx]);
						
						byingPrice = priceHist.get(idx).getClosingPrice();
					}
					
					// If the next last short moving average was above the next last long moving
					// average and the latest short moving average crossed below the last long 
					// moving average, it is a sell signal
					if (shortMaValues[idx -1] >= longMaValues[idx -1] && shortMaValues[idx] < longMaValues[idx] && byingPrice != 0.0f) {
						

						log4j.debug("MA" + shortMaPeriod + "/" + longMaPeriod + "/" + longMaPeriod + " short MA crossover long MA SELL signal for " + 
					            stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + " with price: " + 
								priceHist.get(idx).getClosingPrice()); 
						log4j.debug("Last buy/sell result: " + priceHist.get(idx).getClosingPrice() + "-" + byingPrice + " = " + 
								(priceHist.get(idx).getClosingPrice() - byingPrice));
						
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;
						
						log4j.trace("prevShortMaValue >= prevLongMaValue : " + shortMaValues[idx -1] + " >= " + longMaValues[idx -1]);
						log4j.trace("currShortMaValue < currLongMaValue : " + shortMaValues[idx] + " < " + longMaValues[idx]);

						byingPrice = 0.0f;
					}
					
					// If the initial stoploss triggers, sell the stock
					if (byingPrice != 0.0f && priceHist.get(idx).getClosingPrice() < (byingPrice * (1 - ((float)argInitStopLoss / 100))) ) {
						
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

						log4j.debug("MA" + shortMaPeriod + "/" + longMaPeriod + "/" + longMaPeriod + " Stop loss triggered for " + 
					            stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + " with price: " + 
								priceHist.get(idx).getClosingPrice()); 
						log4j.debug("Last buy/sell result: " + priceHist.get(idx).getClosingPrice() + "-" + byingPrice + " = " + 
					                (priceHist.get(idx).getClosingPrice() - byingPrice));
						log4j.trace("prevShortMaValue >= prevLongMaValue : " + shortMaValues[idx -1] + " >= " + longMaValues[idx -1]);
						log4j.trace("currShortMaValue < currLongMaValue : " + shortMaValues[idx] + " < " + longMaValues[idx]);

						byingPrice = 0.0f;
					}
					idx++;
				}
				log4j.info(stock.getCompanyName() + ", total winning price for: " + stock.getCompanyName() + " is " + totalWinnings); 
			}				
		}
	} // end backTrade()

	public void latestSignal(Stock stock) {} // end latestSignal()
	
} // end class StockFilter
