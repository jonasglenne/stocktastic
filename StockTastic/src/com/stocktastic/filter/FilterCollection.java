package com.stocktastic.filter;

import java.util.List;

/**
 * Class that represents a list of filters. It may be a pre-defined or
 * a user defined list of filters.
 * 
 * @author stationaradm
 */
public class FilterCollection {
	
	
	private String collectionName;
	
	private List<String> filterCollection;
	
	
	/**
	 * Constructor for class FilterCollection
	 */
	public FilterCollection(String collectionName, List<String> filterCollection) {
		this.collectionName = collectionName;
		this.filterCollection = filterCollection;
	} // end constructor StockCollection()


	public String getCollectionName() {
		return collectionName;
	}


	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}


	public List<String> getStockCollection() {
		return filterCollection;
	}

} // end class FilterCollection