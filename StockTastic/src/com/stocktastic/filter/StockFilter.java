package com.stocktastic.filter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Stock;

/**
 * Abstract class that represents a filter. Contains the filter name
 * and basic functionality. Override applyFilter() to define behavior
 * 
 * @author stationaradm
 */
public abstract class StockFilter {

	private static Logger log4j;

	// Full name of the filter
	private String filterName;
	
	// Description of the filter
	private String description;
	
	// The price history for a stock.
	ArrayList<PriceListing> priceHist;
	
	// The stock this filter shall be applied to 
	Stock stock;
	
	protected enum Perspective {
		ULTRASHORT(5), SHORT(21), INTERMEDIATE(90), LONG(365);
		private final int length;
		private Perspective(int length) {
            this.length = length;
		}
		public int getValue() {
	        return length;
	    }
	}
	
	/**
	 * Constructor for class StockFilter with only the names
	 * of the filter as a parameter.
	 * 
	 * @param argName a String that is the name of the stock
	 */
	public StockFilter(String filterName, String description) {
		this.filterName = filterName;
		this.description = description;
		
		log4j = LogManager.getLogger(StockFilter.class.getName());
	} // end constructor StockFilter()
	
	
	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Checks the if the applied filter triggered a buy/sell signal
	 * the latest available date
	 * 
	 * @param stock a Stock that is the stock to apply this filter to
	 * @param continueUntilSignalFound a boolean indicating if only checking the latest available date or latest signal
	 */
	public abstract void applyFilter(Stock stock, boolean continueUntilSignalFound);
		
	/**
	 * Applies the selected filter to the total history. I.e., it will
	 * start from the first available date and purchase the stock at that
	 * day and then check the outcome of following a buy/sell strategy based
	 * on this filter from each possible starting date.
	 * 
	 * @param argStartDate a Calendar that represents from which date to start trading from, if null start from the first available day in price history.
	 * @param argStock a Stock that is the selected stock
	 * @param argInitStopLoss an integer that represents the accepted initial loss in percent for the trade
	 * @param argTrailStopLoss an integer that represents the trailing stop loss when a trade has reached breakeven.
	 */
	public abstract void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss);

	/**
	 * If the user has supplied a start date for the backtrade, check that it is valid here and
	 * set the startingposition to that date.
	 * 
	 * @param argStartDate a Calendar that holds the selected start date
	 * @return an int that is the index in the price history where to start the back trade from
	 */
	protected int checkStartDate(Calendar argStartDate) {

		int dateIndex = -1;
		
		boolean startDateFound = false;
		Date startDate = argStartDate.getTime();
		
		for (int idx = 0; idx < priceHist.size() && startDateFound == false; idx++) {
			if (priceHist.get(idx).getDate().equals(startDate)) {
				startDateFound = true;
				dateIndex = idx;
			}
		}
		
		if (startDateFound == false) {
			log4j.error("Aborting backtrading, provided start date: {} for stock {} could not be found in the price history.", 
				        startDate, stock.getCompanyName());
			log4j.error("Earliest available date: {} closest day forward in time from given date.", 
						priceHist.get(0).getDate());
		}

		return dateIndex;
	}
	
	public void fullBackTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		log4j.error("Method fullBackTrade() has not been implemented for this filter {}.", getFilterName());
	}
} // end class StockFilter
