package com.stocktastic.filter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;

/**
 * Filter that aims to buy when fast MA crosses a slow MA and sell when a fast MA
 * crosses a medium slow MA.
 * 
 * @author stationaradm
 */
public class MASlowBuyFastSell extends StockFilter {
	
	// Represents the number of days to calculate the fast moving average value
	private int fastMaPeriod;
	// Represents the number of days to calculate the medium long moving average value
	private int mediumMaPeriod;
	// Represents the number of days to calculate the long moving average value
	private int longMaPeriod;
	// Represents the initial stop loss for a position. Used for backtrading
	private int initialStopLoss;
	// Represents the trailing stop loss for a position. Used for backtrading
	private int trailStopLoss;
	
	private float[] shortMaValues;
	private float[] mediumMaValues;
	private float[] longMaValues;

	private static Logger log4j;

	/**
	 * Constructor for class MASlowBuyFastSell
	 * 
	 * @param shortMa an Integer that represents the number of days to calculate the moving average value
	 * @param longMaPeriod an Integer that represents the number of days to calculate the moving average value
	 */
	public MASlowBuyFastSell(int argFastMaPeriod, int argMediumMaPeriod, int argLongMaPeriod) {
		super("MA Slow Buy Fast Sell", "This filter calculates buy/sell signals using three different MA values, one fast, one medium and one slow.");
		fastMaPeriod = argFastMaPeriod;
		mediumMaPeriod = argMediumMaPeriod;
		longMaPeriod = argLongMaPeriod;
		
		// Since the filter name is variable with the periods, update the filter name (it is used for comparison in Signal objects).
		setFilterName("MA Slow Buy Fast Sell " + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod);

		log4j = LogManager.getLogger(MASlowBuyFastSell.class.getName());
	} // end constructor MAFilter

	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		
		Date currentDate = StockTasticUtils.getCurrentDate();	
		stock = argStock;
		priceHist = stock.getPrices();
		boolean signalFound = false;
		
		shortMaValues = StockTasticUtils.calcMaValues(priceHist, fastMaPeriod);
		mediumMaValues = StockTasticUtils.calcMaValues(priceHist, mediumMaPeriod);
		longMaValues = StockTasticUtils.calcMaValues(priceHist, longMaPeriod);
		
		// Make sure we have at least the number of price points + 1 in history as the long
		// moving average in order to be able to calculate a valid MA value for both today
		// and yesterday for the selected range.
		if (priceHist.size() > longMaPeriod + 1) {
			
			int idx = priceHist.size() -1;

			do {
				
				// If the short MA value crosses the long MA value from below and the short MA value was below the 
				// yesterday's long MA value it is a BUY signal
				if (shortMaValues[idx - 1] <= longMaValues[idx - 1] && shortMaValues[idx] > longMaValues[idx]) {
					
					long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(idx).getDate());
					
					log4j.info(differenceInDays + " day(s) old. MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
							   " short MA crossover long MA BUY signal for " + stock.getCompanyName() + " at date: " + 
							   priceHist.get(idx).getShortDateString()); 
					log4j.trace("Current short MA: " + shortMaValues[idx] + ", previous short MA: " + shortMaValues[idx - 1]); 
					log4j.trace("Current long MA: " + longMaValues[idx] + ", previous long MA: " + longMaValues[idx - 1]);
					log4j.trace("Current closing price: " + priceHist.get(idx).getClosingPrice());
					
					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(idx).getDate(), 
							differenceInDays + " day(s) old MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
							" short MA crossover long MA BUY signal for " + stock.getCompanyName() + " at date: " + 
							priceHist.get(idx).getShortDateString() + " with closing price: " + 
							priceHist.get(idx).getClosingPrice(), SignalType.BUY));
					
					signalFound = true;
				}
				
				// If the short MA value crosses the medium MA value from above and yesterday's the short MA value was above the 
				//medium MA value, it is a SELL signal.
				if (shortMaValues[idx - 1] >= mediumMaValues[idx - 1] && shortMaValues[idx] < mediumMaValues[idx]) {

					long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(idx).getDate());
					log4j.info(differenceInDays + " day(s) old. MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
							   " short MA crossover medium MA SELL signal for " + stock.getCompanyName() + " at date: " + 
							   priceHist.get(idx).getShortDateString()); 
					log4j.trace("Current short MA: " + shortMaValues[idx] + ", previous short MA: " + shortMaValues[idx - 1]); 
					log4j.trace("Current medium MA: " + mediumMaValues[idx] + ", previous medium MA: " + mediumMaValues[idx - 1]);
					log4j.trace("Current closing price: " + priceHist.get(idx).getClosingPrice()); 
					
					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(idx).getDate(), 
							differenceInDays + " day(s) old MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
							" short MA crossover medium MA SELL signal for " + stock.getCompanyName() + " at date: " + 
							priceHist.get(idx).getShortDateString() + " with closing price: " + priceHist.get(idx).getClosingPrice(),
							SignalType.SELL));

					signalFound = true;
				}
				
				// If the short MA value crosses the medium MA value from below and yesterday's the short MA value was above the 
				//medium MA value, after the price has caused a sell signal when the short MA value has crossed the medium MA value
				// from above it is a BUY signal.
				if (shortMaValues[idx - 1] <= mediumMaValues[idx - 1] && shortMaValues[idx] > mediumMaValues[idx] && 
					mediumMaValues[idx] > longMaValues[idx]) {

					// Check that the closest previous signal in the past was a SELL signal from crossing the medium MA value from 
					// above with the short MA value to make this signal possible.
					int lastSignalPosition = idx - 1;
					while (lastSignalPosition > longMaPeriod) {
						
						if (shortMaValues[lastSignalPosition - 1] >= mediumMaValues[lastSignalPosition - 1] && 
							shortMaValues[lastSignalPosition] < mediumMaValues[lastSignalPosition]) {
							signalFound = true;
							break;
						}
						lastSignalPosition--;
					}
					
					long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(idx).getDate());
					log4j.info(differenceInDays + " day(s) old. MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
							   " short MA crossover medium MA BUY signal for " + stock.getCompanyName() + " at date: " + 
							   priceHist.get(idx).getShortDateString()); 
					log4j.trace("Current short MA: " + shortMaValues[idx] + ", previous short MA: " + shortMaValues[idx - 1]); 
					log4j.trace("Current medium MA: " + mediumMaValues[idx] + ", previous medium MA: " + mediumMaValues[idx]);
					log4j.trace("Current closing price: " + priceHist.get(idx).getClosingPrice()); 
					
					stock.addSignal(new Signal(getFilterName(), priceHist.get(idx).getDate(), differenceInDays + 
							" day(s) old MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
							" short MA crossover medium MA BUY signal for " + stock.getCompanyName() + 
							" at date: " + priceHist.get(idx).getShortDateString() + " with closing price: " + 
							priceHist.get(idx).getClosingPrice(),
							SignalType.BUY));

					signalFound = true;
				}

				idx--;
				
			} while (continueUntilSignalFound == true && idx > longMaPeriod && signalFound == false);

		}
		else {
			log4j.error("Not enough price history to calculate moving average for " + stock.getCompanyName() +", required: " + 
		                 longMaPeriod + ", found: " + (priceHist.size() -1)); 
		}
	} // end applyFilter()

	@Override
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		initialStopLoss = argInitStopLoss;
		trailStopLoss = argTrailStopLoss;
		
		stock = argStock;
		priceHist = stock.getPrices();
		
		float byingPrice = 0.0f;
		float totalWinnings = 0.0f;
		
		shortMaValues = StockTasticUtils.calcMaValues(priceHist, fastMaPeriod);
		mediumMaValues = StockTasticUtils.calcMaValues(priceHist, mediumMaPeriod);
		longMaValues = StockTasticUtils.calcMaValues(priceHist, longMaPeriod);

		// Make sure we have at least the selected long MA-period + 1 price points in history in order to be able to
		// calculate the MA value.
		if (priceHist.size() >= (longMaPeriod + 1) ) {
			
			// Start at the long MA Period position in the price history.
			int idx = longMaPeriod;
			
			if (argStartDate != null) {
				idx = checkStartDate(argStartDate) + 1;
			}
			if (idx > -1) {
				while (idx < priceHist.size()) {
					
					// If the short MA value crosses the long MA value from below and the short MA value was below the 
					// yesterday's long MA value, and, in addition the stock is not currently held, it is a BUY signal.
					if (shortMaValues[idx - 1] <= longMaValues[idx - 1] && shortMaValues[idx] > longMaValues[idx] && byingPrice == 0.0f) {
						
						log4j.debug("MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + " short MA crossover long MA BUY signal for " + 
						            stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + " with price: " + 
						            priceHist.get(idx).getClosingPrice()); 
						log4j.trace("Previous short MA < previous long MA : " + shortMaValues[idx - 1] + " < " + longMaValues[idx - 1]);
						log4j.trace("Current short MA > current long MA : " + shortMaValues[idx] + " > " + longMaValues[idx]);

						byingPrice = priceHist.get(idx).getClosingPrice();
					}
					
					// If the short MA value crosses the medium MA value from above and the short MA value was above the 
					// yesterday's medium MA value, and, in addition the stock is currently held, it is a SELL signal.
					if (shortMaValues[idx - 1] >=  mediumMaValues[idx - 1] && shortMaValues[idx] < mediumMaValues[idx] && byingPrice != 0.0f) {
						
						log4j.debug("MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + " short MA crossover long MA SELL signal for " + 
					            	stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + " with price: " + 
					            	priceHist.get(idx).getClosingPrice()); 
						log4j.debug("Last buy/sell result: " + priceHist.get(idx).getClosingPrice() + "-" + byingPrice + " = " + 
									(priceHist.get(idx).getClosingPrice() - byingPrice));
						
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;
						byingPrice = 0.0f;
						
						log4j.trace("Previous short MA >= previous medium MA : " + shortMaValues[idx - 1] + " >= " + mediumMaValues[idx - 1]);
						log4j.trace("Current short MA < current medium MA : " + shortMaValues[idx] + " < " + mediumMaValues[idx]);
					}

					// If the short MA value crosses the medium MA value from below and yesterday's the short MA value was above the 
					//medium MA value, after the price has caused a sell signal when the short MA value has crossed the medium MA value
					// from above it is a BUY signal.
					if (shortMaValues[idx - 1] <= mediumMaValues[idx - 1] && shortMaValues[idx] > mediumMaValues[idx] && 
						mediumMaValues[idx] > longMaValues[idx] && byingPrice == 0.0f) {

						// Check that the closest previous signal in the past was a SELL signal from crossing the medium MA value from 
						// above with the short MA value to make this signal possible.
						int lastSignalPosition = idx - 1;
						boolean prevSignalWasSell = false;
						
						while (lastSignalPosition > longMaPeriod) {
							
							if (shortMaValues[lastSignalPosition - 1] >= mediumMaValues[lastSignalPosition - 1] && 
								shortMaValues[lastSignalPosition] < mediumMaValues[lastSignalPosition]) {
								prevSignalWasSell = true;
								break;
							}
							lastSignalPosition--;
						}
						
						if (prevSignalWasSell == true) {
							log4j.debug("MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
										" short MA crossover medium MA BUY signal for " + stock.getCompanyName() + " at date: " + 
										priceHist.get(idx).getShortDateString() + " with price: " + priceHist.get(idx).getClosingPrice());
							log4j.trace("Previous short MA > previous medium MA : " + shortMaValues[idx - 1] + " < " + mediumMaValues[idx -1]);
							log4j.trace("Current short MA > current medium MA : " + shortMaValues[idx] + " > " + mediumMaValues[idx]);
							
							byingPrice = priceHist.get(idx).getClosingPrice();
						}
					}
					idx++;
				}
				log4j.info(stock.getCompanyName() + ", total winning price: " + totalWinnings); 				
			}
		}
	} // end backTrade()

	
	public void fullBackTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		initialStopLoss = argInitStopLoss;
		trailStopLoss = argTrailStopLoss;
		
		stock = argStock;
		priceHist = stock.getPrices();
		
		shortMaValues = StockTasticUtils.calcMaValues(priceHist, fastMaPeriod);
		mediumMaValues = StockTasticUtils.calcMaValues(priceHist, mediumMaPeriod);
		longMaValues = StockTasticUtils.calcMaValues(priceHist, longMaPeriod);

		// Start at the long MA Period position in the price history.
		int startDateIdx = longMaPeriod;
		
		ArrayList<FilterResult> results = new ArrayList<FilterResult>();

		if (argStartDate != null) {
			startDateIdx = checkStartDate(argStartDate) + 1;
		}
		
		// Make sure we have at least the selected long MA-period + 1 price points in history in order to be able to
		// calculate the MA value.
		if (startDateIdx > -1 && priceHist.size() >= (longMaPeriod + 1)) {
			
			// Loop through all levels of initial stop losses (1 to 5 %)
			for (int initialStopLoss = 1; initialStopLoss < 6; initialStopLoss++) {
			
				// For each level of initial stop loss, loop through all levels of trailing stop loss (1 to 5 %)
				for (int trailStopLoss = 1; trailStopLoss < 6; trailStopLoss++) {
					
					// This is the offset from the start of the price history. For each combination
					// of initial and trailing stop loss, the backtrading starts from the first available 
					// date in the price history and after running through the complete price history, start 
					// from the second oldest price history date and repeat until the last avaiable date has 
					// been reached in the price history.
					int offset = 0;

					//Statistics for this combination of initial and trailing stop loss
					float worstResult = 0.0f;
					float bestResult = 0.0f;
					Date worstResultStartDate = argStartDate.getTime();
					Date bestResultStartDate = argStartDate.getTime();

					// Sum for medium value
					float totalSum = 0.0f;
					// Divisor for the medium value.
					int numIterations = 0;
					
					// For each combination of initial and trailing stop loss e.g. 1,1 1,2 ,1,3 2,1, 2,2 2,3 3,1, 3,2 3,3
					// Loop through the complete price history and start at each day trying out the combination for the complete
					// price history from start data idx + offset to today
					while ( (startDateIdx + offset) < priceHist.size() ) {
						
						// The current bying price for the stock
						float byingPrice = 0.0f;
						// The total winnings from the filter.
						float totalWinnings = 0.0f;
						// The price that will trigger a trailing stop loss.
						float trailStopLossPrice = 0.0f;
						// The price level when the trailing stop will be raised.
						float trailStopLossLevelRaisePrice = 0.0f;
						
						// Apply the filter to the specific combination of inital and traling stop losses at from this specific start date.
						for (int innerIdx = startDateIdx + offset; innerIdx < priceHist.size(); innerIdx++) {

							// If the short MA value crosses the long MA value from below and the short MA value was below the 
							// yesterday's long MA value, and, in addition the stock is not currently held, it is a BUY signal.
							if (shortMaValues[innerIdx - 1] <= longMaValues[innerIdx - 1] && 
								shortMaValues[innerIdx] > longMaValues[innerIdx] && 
								byingPrice == 0.0f) {
								
								log4j.debug("MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + " short MA crossover long MA BUY signal for " + 
								            stock.getCompanyName() + " at date: " + priceHist.get(innerIdx).getShortDateString() + " with price: " + 
								            priceHist.get(innerIdx).getClosingPrice()); 
								log4j.trace("Previous short MA < previous long MA : " + shortMaValues[innerIdx - 1] + " < " + longMaValues[innerIdx - 1]);
								log4j.trace("Current short MA > current long MA : " + shortMaValues[innerIdx] + " > " + longMaValues[innerIdx]);

								byingPrice = priceHist.get(innerIdx).getClosingPrice();
							}
							
							// If the short MA value crosses the medium MA value from above and the short MA value was above the 
							// yesterday's medium MA value, and, in addition the stock is currently held, it is a SELL signal.
							if (byingPrice != 0.0f && shortMaValues[innerIdx - 1] >=  mediumMaValues[innerIdx - 1] && 
								shortMaValues[innerIdx] < mediumMaValues[innerIdx]) {
								
								log4j.debug("MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + " short MA crossover long MA SELL signal for " + 
							            	stock.getCompanyName() + " at date: " + priceHist.get(innerIdx).getShortDateString() + " with price: " + 
							            	priceHist.get(innerIdx).getClosingPrice()); 
								log4j.debug("Last buy/sell result: " + priceHist.get(innerIdx).getClosingPrice() + "-" + byingPrice + " = " + 
											(priceHist.get(innerIdx).getClosingPrice() - byingPrice));
								
								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;
								byingPrice = 0.0f;
								trailStopLossPrice = 0.0f;
								trailStopLossLevelRaisePrice = 0.0f;
								
								log4j.trace("Previous short MA >= previous medium MA : " + shortMaValues[innerIdx - 1] + " >= " + mediumMaValues[innerIdx - 1]);
								log4j.trace("Current short MA < current medium MA : " + shortMaValues[innerIdx] + " < " + mediumMaValues[innerIdx]);
							}
							
							// If the short MA value crosses the medium MA value from below and yesterday's the short MA value was above the 
							//medium MA value, after the price has caused a sell signal when the short MA value has crossed the medium MA value
							// from above it is a BUY signal.
							if (shortMaValues[innerIdx - 1] <= mediumMaValues[innerIdx - 1] && shortMaValues[innerIdx] > mediumMaValues[innerIdx] && 
								mediumMaValues[innerIdx] > longMaValues[innerIdx] && byingPrice == 0.0f) {

								// Check that the closest previous signal in the past was a SELL signal from crossing the medium MA value from 
								// above with the short MA value to make this signal possible.
								int lastSignalPosition = innerIdx - 1;
								boolean prevSignalWasSell = false;
								
								while (lastSignalPosition > longMaPeriod) {
									
									if (shortMaValues[lastSignalPosition - 1] >= mediumMaValues[lastSignalPosition - 1] && 
										shortMaValues[lastSignalPosition] < mediumMaValues[lastSignalPosition]) {
										prevSignalWasSell = true;
										break;
									}
									lastSignalPosition--;
								}
								
								if (prevSignalWasSell == true) {
									log4j.debug("MA" + fastMaPeriod + "/" + mediumMaPeriod + "/" + longMaPeriod + 
												" short MA crossover medium MA BUY signal for " + stock.getCompanyName() + " at date: " + 
												priceHist.get(innerIdx).getShortDateString() + " with price: " + priceHist.get(innerIdx).getClosingPrice());
									log4j.trace("Previous short MA > previous medium MA : " + shortMaValues[innerIdx - 1] + " < " + mediumMaValues[innerIdx -1]);
									log4j.trace("Current short MA > current medium MA : " + shortMaValues[innerIdx] + " > " + mediumMaValues[innerIdx]);
									
									byingPrice = priceHist.get(innerIdx).getClosingPrice();
								}
							}
							
							// If the initial stoploss triggers, sell the stock
							if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() < (byingPrice * (1 - ((float)initialStopLoss / 100))) ) {
								
								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;

								log4j.debug("Initial stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(innerIdx).getClosingPrice(), 
											byingPrice, (priceHist.get(innerIdx).getLowestPrice() - byingPrice));
								log4j.debug("Initial stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
										     priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());

								byingPrice = 0.0f;
								trailStopLossPrice = 0.0f;
								trailStopLossLevelRaisePrice = 0.0f;
							}
							
							// Initiate the trailing stop loss If the price reaches a break even (initial price + stopinitial stoploss.
							if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() > (byingPrice * (1 + ((float)initialStopLoss / 100))) ) {

								// Set the current trailing stop loss level.
								trailStopLossPrice = priceHist.get(innerIdx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
								// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
								// TODO Evaluate how the raising of this level can be optimised.
								trailStopLossLevelRaisePrice = priceHist.get(innerIdx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));

								log4j.debug("Trailing stop loss initiated for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());
							}
							
							// Increase the trailing stop loss if the price increases with the initial stop loss percentage again.
							if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() > trailStopLossLevelRaisePrice ) {

								// Set the current trailing stop loss level.
								trailStopLossPrice = priceHist.get(innerIdx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
								// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
								// TODO Evaluate how the raising of this level can be optimised.
								trailStopLossLevelRaisePrice = priceHist.get(innerIdx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));
								
								log4j.debug("Trailing stop loss raised for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());
							}
							
							// If the price reaches the trailing stop loss trigger a sell.
							if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && (priceHist.get(innerIdx).getClosingPrice() < trailStopLossPrice) ) {

								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;

								log4j.debug("Trailing stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(innerIdx).getClosingPrice(), 
											byingPrice, (priceHist.get(innerIdx).getClosingPrice() - byingPrice));
								log4j.debug("Trailing stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());

								byingPrice = 0.0f;
								trailStopLossPrice = 0.0f;
								trailStopLossLevelRaisePrice = 0.0f;
							}							
						}
						if (worstResult > totalWinnings) {
							worstResult = totalWinnings;
							worstResultStartDate = priceHist.get(startDateIdx + offset).getDate();
						}
						if (bestResult < totalWinnings) {
							bestResult = totalWinnings;
							bestResultStartDate = priceHist.get(startDateIdx + offset).getDate();
						}
						
						// Add to the variables for medium calculation for the current start date
						totalSum += totalWinnings;
						numIterations += 1;
						
						log4j.debug("Total winning price starting from " + priceHist.get(startDateIdx + offset).getShortDateString() + " for " + stock.getCompanyName() + 
									": " + totalWinnings);	
						offset += 1;
					}
					
					//Calculate the medium winnings/loss from this iteration
					float mediumResult = totalSum / numIterations;
					
					// Add the results from this iteration to the total results
					FilterResult isIt = new FilterResult(initialStopLoss, trailStopLoss, bestResult, worstResult, mediumResult, bestResultStartDate, worstResultStartDate);
					results.add(isIt);
				}
			}
		
			log4j.info( "Result of full backtrade for filter {} on stock {}", getFilterName(), stock.getCompanyName());
			log4j.info( "---------------------------------------------------------------------------------------");
			log4j.info( "Initial Trailing Best Date	Best	Worst Date	Worst	Medium");
			log4j.info( "---------------------------------------------------------------------------------------");
			
			for (FilterResult result : results) {
				log4j.info(result.toString());
			}
			log4j.info( "---------------------------------------------------------------------------------------");
		}
	} // end fullBackTrade()

		
	public void latestSignal(Stock stock) {} // end latestSignal()
	
} // end class MASlowBuyFastSell
