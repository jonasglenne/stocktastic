package com.stocktastic.filter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;

/**
 * Class that represents a filter för MA stock closing price cross over 
 * signals.
 * 
 * Basic concept is that when the closing price closes above the specified
 * MA(X) value, it is a BUY signal. If the closing price of the stock closes
 * below the MA(X) value, it is considered a SELL signal.
 * 
 * @author stationaradm
 */
public class MAFilter extends StockFilter {
	
	// Represents the number of days to calculate the moving average value
	private int maPeriod;
	private static Logger log4j;
	
	private float[] maValues;
	
	/**
	 * Constructor for class StockFilter with only the name
	 * of the filter as a parameter.
	 * 
	 * @param maPeriod an Integer that represents the number of days to calculate the moving average value
	 */
	public MAFilter(int argMaPeriod) {
		super("MA Price Crossover", "Signals when a stocks closing price crosses the specified moving average");
		this.maPeriod = argMaPeriod;
		
		// Since the filter name is variable with the periods, update the filter name (it is used for comparison in Signal objects).
		setFilterName("MA Price Crossover " + maPeriod);

		log4j = LogManager.getLogger(MADoubleCrossOverFilter.class.getName());
	} // end constructor MAFilter()

	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		maValues = StockTasticUtils.calcMaValues(priceHist, maPeriod);
		
		Date currentDate = StockTasticUtils.getCurrentDate();	
		boolean signalFound = false;
		
		// Make sure we have at least the number of price points + 1 in history as the long
		// moving average in order to be able to calculate a valid MA value for both today
		// and yesterday for the selected range.
		if (priceHist.size() > maPeriod + 1) {
			
			int idx = priceHist.size() -1;

			do {

				long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(idx).getDate());
				
				// If the current closing price is above the MA(X) value and the yesterday closing price was below the MA(X)
				// value a BUY signal has been generated.
				if (priceHist.get(idx).getClosingPrice() > maValues[idx] && 
					priceHist.get(idx -1).getClosingPrice() <= maValues[idx -1]) {
					
					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(idx).getDate(), 
							differenceInDays + " day(s) old MA" + maPeriod + " closing price crossover BUY signal for " + 
							stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + 
							"with closing price:" + priceHist.get(idx).getClosingPrice() + " and MA" + maPeriod + 
							" at: " + maValues[idx],
							SignalType.BUY));

					log4j.debug("{} day(s) old MA{} stock closing price crossover BUY signal for {} at date: {} with closing price: {} and MA{} at: {}",  
							          differenceInDays, maPeriod, stock.getCompanyName(), priceHist.get(idx).getShortDateString(), 
							          priceHist.get(idx).getClosingPrice(), maPeriod, maValues[idx]);
					signalFound = true;
				}
				
				//6. If the next last short moving average was above the next last long moving
				//   average and the latest short moving average crossed below the last long 
				//   moving average, it is a sell signal
				if (priceHist.get(idx).getClosingPrice() < maValues[idx] &&
				   priceHist.get(idx -1).getClosingPrice() >= maValues[idx -1]) {
					
					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(idx).getDate(), 
							differenceInDays + " day(s) old. MA" + maPeriod + " closing price crossover SELL signal for " + 
							stock.getCompanyName() + " at date: " + priceHist.get(idx).getShortDateString() + "with closing price:" + 
							priceHist.get(idx).getClosingPrice() + " and MA" + maPeriod + " at: " + maValues[idx], SignalType.SELL));

					log4j.debug("{} day(s) old MA{} stock closing price crossover SELL signal for {} at date: {} with closing price: {} and MA{} at: {}", 
								  	differenceInDays, maPeriod, stock.getCompanyName(), priceHist.get(idx).getShortDateString(), 
								  	priceHist.get(idx).getClosingPrice(), maPeriod, maValues[idx]);

					signalFound = true;
				}

				log4j.trace("Current MA value was %f and price was: %f%n",  maValues[idx], priceHist.get(idx).getClosingPrice());
				log4j.trace("Previous MA value was %f and price was: %f%n",  maValues[idx -1], priceHist.get(idx).getClosingPrice() - 1);
				log4j.trace("Counting date: %s%n", priceHist.get(idx).getShortDateString());

				idx--;
				
			} while (continueUntilSignalFound == true && idx > maPeriod && signalFound == false);

		}
		else {
			log4j.warn("Not enough price history to calculate moving average for %s,  required: %d, found: %d%n", 
						      stock.getCompanyName(), maPeriod , (priceHist.size() -1));
		}
	} // end applyFilter()

	@Override
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		maValues = StockTasticUtils.calcMaValues(priceHist, maPeriod);
		
		float byingPrice = 0.0f;
		float totalWinnings = 0.0f;
		boolean trailStopLossActive = false;
		
		// Make sure we have at least the selected long MA-period + 1 price points in history in order to be able to
		// calculate the MA value closing price crossover.
		if (priceHist.size() > maPeriod + 1) {
			
			// Start at the MA Period position to be able to calculate MA for the first iteration of a full period + the previous
			// period, e.g. for a MA5 positions 1,2,3,4,5 will be summed and divided by 5 and also position 0,1,2,3,4 will be summed
			// and divided to be able to check if the MA closing price crossover signal is triggered.
			int idx = maPeriod;
			
			if (argStartDate != null) {
				idx = checkStartDate(argStartDate);
			}
			if (idx > -1) {
				
				// Repeat throughout the available price history to collect all signals.
				while (idx < priceHist.size()) {

					// If the price for the current position is above the MA(X) and the previous day's closing price was below the previous day's
					// MA(X) we have a BUY signal triggered for closing price MA crossover. Also make sure that we are not already holding
					// the stock which is indicated by byingPrice == 0
					if (priceHist.get(idx).getClosingPrice() > maValues[idx] && 
						priceHist.get(idx - 1).getClosingPrice() < maValues[idx -1] && 
						byingPrice == 0.0f) {
						
						byingPrice = priceHist.get(idx).getClosingPrice();

						log4j.debug("MA{} stock closing price crossover BUY signal for {} at date: {} with price: {}", maPeriod, 
								    stock.getCompanyName(), priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());
						log4j.trace("currDayMaValue: {}", maValues[idx]);
						log4j.trace("currDayMaValue: {}", maValues[idx -1]);
					}
					
					// If the closing price is below the MA(X) and the previous day's closing price was above the previous day's MA(X) we have 
					// a SELL signal. Also make sure that we are currently holding the stock to be able to sell it, indicated by byingPrice != 0
					else if (priceHist.get(idx).getClosingPrice() < maValues[idx] && 
							 priceHist.get(idx - 1).getClosingPrice() > maValues[idx -1] && 
							 byingPrice != 0.0f && trailStopLossActive == false) {
						
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

						log4j.debug("SELL signal: MA{} stock closing price crossover SELL signal for {} at date: {} with price: {}", maPeriod, 
							        stock.getCompanyName(), priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());
						log4j.debug("SELL signal: Last buy/sell result: {} - {} = {}",priceHist.get(idx).getLowestPrice(), byingPrice, 
								priceHist.get(idx).getClosingPrice() - byingPrice);

						log4j.trace("currDayMaValue: %f%n", maValues[idx]);
						log4j.trace("currDayMaValue: %f%n", maValues[idx -1]);

						trailStopLossActive = false;
						byingPrice = 0.0f;
					}
					
					// If the initial stoploss triggers, SELL the stock (provided that it is currently held, indicated by byingPrice != 0
					else if (priceHist.get(idx).getClosingPrice() < (byingPrice * (1 - ((float)argInitStopLoss / 100))) &&
							 byingPrice != 0.0f && trailStopLossActive != true) {
						
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

						log4j.debug("Stoploss: MA{} Stop loss triggered!!! for {} at date: {} with price: {}", maPeriod, 
						          stock.getCompanyName(), priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());
						log4j.debug("Stoploss: last buy/sell result: {} - {} = {}",priceHist.get(idx).getClosingPrice(), byingPrice, 
								priceHist.get(idx).getClosingPrice() - byingPrice);

						log4j.trace("currDayMaValue: {}", maValues[idx]);
						log4j.trace("currDayMaValue: {}", maValues[idx -1]);

						byingPrice = 0.0f;
					}
					
					// If we break even on our bet, activate the trailing stop loss. The intention is to minimize losses if we are lucky
					// enough not to lose anything from the initial bet.
					else if (priceHist.get(idx).getClosingPrice() > byingPrice * (1 + ((float)argTrailStopLoss / 100)) &&
							 trailStopLossActive == false && 
							 byingPrice != 0.00) {
						
						trailStopLossActive = true;
						
						log4j.debug("Trailing Stoploss ACTIVATED for {} at date: {} with price: {}", 
						          stock.getCompanyName(), priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());
					}
					
					// If the trailing stop loss is active and the closing price declines 2% from the current levelm, SELL the stock. This is if we are
					// holding the stock which is given by byingPrice != 0.00
					else if ( (priceHist.get(idx).getClosingPrice() / priceHist.get(idx -1).getClosingPrice()) <  (1 - ((float)argTrailStopLoss / 100)) &&
							 trailStopLossActive == true && 
							 byingPrice != 0.00) {
						
						// TODO Very defensive to assume the worst, should this be the closing price instead?
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

						log4j.debug("Trailing Stoploss: MA{} Trailing Stop loss triggered!!! for {} at date:{} with price: {}", maPeriod, 
						          stock.getCompanyName(), priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());
						log4j.debug("Stoploss: last buy/sell result: {} - {} = {}", priceHist.get(idx).getClosingPrice(), byingPrice, 
								priceHist.get(idx).getLowestPrice() - byingPrice);

						log4j.trace("currDayMaValue: {}", maValues[idx]);
						log4j.trace("currDayMaValue: {}", maValues[idx -1]);

						byingPrice = 0.0f;
						trailStopLossActive = false;
					}
					else {
						log4j.trace("Nothing happened today: {}",priceHist.get(idx).getDate());
					}
					idx++;
				}
				log4j.info("Total winning price for {}: {}",stock.getCompanyName() , totalWinnings);
			}				
		}
	} // end backTrade()

	public void latestSignal(Stock stock) {} // end latestSignal()
	
} // end class MAFilter
