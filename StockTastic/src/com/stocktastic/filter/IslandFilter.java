package com.stocktastic.filter;

import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.filter.TrendCalculator.Trend;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;

/**
 * Abstract class that represents a filter. Contains the filter name
 * and basic functionality. Override applyFilter() to define behavior
 * 
 * @author stationaradm
 */
public class IslandFilter extends StockFilter {
	
	private static Logger log4j;

	/**
	 * Constructor for class StockFilter with only the name
	 * of the filter as a parameter.
	 * 
	 * @param argName a String that is the name of the stock
	 */
	public IslandFilter() {
		super("Island Reversal", "When the price forms a gap for the day before and after.");
		
		log4j = LogManager.getLogger(IslandFilter.class.getName());
	}

	// TODO Remember to compare to yesterday as well. Yesterday should be below MA50 to generate buy
	//      signal. Yesterdaya should be above MA50 and go below to generate sell signal. As it is 
	//      implemented now. All days it is over or under MA50 will print.
	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		int priceHistPosition = priceHist.size() -1;
		Date currentDate = StockTasticUtils.getCurrentDate();	

		do {
			
		long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(priceHistPosition).getDate());

		int yesterday = priceHistPosition -2;
		int islandDay = priceHistPosition -1;
		int today = priceHistPosition;
		
		if (priceHist.get(yesterday).getLowestPrice() > priceHist.get(islandDay).getHighestPrice() &&
			priceHist.get(today).getLowestPrice() > priceHist.get(islandDay).getHighestPrice()) {

			Trend trend = TrendCalculator.simpleTrend(stock, Perspective.ULTRASHORT, today);

			if (trend == Trend.DOWN) {

				stock.addSignal(new Signal(getFilterName(), 
						priceHist.get(priceHistPosition).getDate(), 
						differenceInDays + " day(s) old Island BUY signal for " + stock.getCompanyName() + 
						" at date: " + priceHist.get(priceHistPosition).getShortDateString() + "with closing price: " + 
						priceHist.get(priceHistPosition).getClosingPrice(),
						SignalType.BUY));
				
				log4j.debug(differenceInDays + " day(s) old. Island BUY signal for " + stock.getCompanyName() + 
							"at date: " + priceHist.get(islandDay).getShortDateString());
				
				continueUntilSignalFound = false;
			}
		}
		
		if (priceHist.get(yesterday).getHighestPrice() < priceHist.get(islandDay).getLowestPrice() &&
			priceHist.get(today).getHighestPrice() < priceHist.get(islandDay).getLowestPrice()) {
			
			Trend trend = TrendCalculator.simpleTrend(stock, Perspective.ULTRASHORT, today);

			if (trend == Trend.UP) {

				stock.addSignal(new Signal(getFilterName(), 
						priceHist.get(priceHistPosition).getDate(), 
						differenceInDays + " day(s) old Island SELL signal for " + stock.getCompanyName() + 
						" at date: " + priceHist.get(priceHistPosition).getShortDateString() + "with closing price: " + 
						priceHist.get(priceHistPosition).getClosingPrice(),
						SignalType.SELL));
				
				log4j.debug(differenceInDays + " day(s) old. Island SELL signal for " + stock.getCompanyName() + 
						"at date: " + priceHist.get(islandDay).getShortDateString());
				continueUntilSignalFound = false;
			}
		}
		
		priceHistPosition--;
		} while ((priceHistPosition -2) >= 0 && continueUntilSignalFound);

	} // end applyFilter()


	@Override
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		stock = argStock;
		priceHist = stock.getPrices();
		
		float byingPrice = 0.0f;
		float totalWinnings = 0.0f;
		
		int yesterday = 0;
		int islandDay = 1;
		int today = 2;
		
		if (argStartDate != null) {
			today = checkStartDate(argStartDate) + 1;
		}
		
		if (today > -1) {
			while (today < priceHist.size()) {
				
				if (priceHist.get(yesterday).getLowestPrice() > priceHist.get(islandDay).getHighestPrice() &&
						priceHist.get(today).getLowestPrice() > priceHist.get(islandDay).getHighestPrice()) {
					
					Trend trend = TrendCalculator.simpleTrend(stock, Perspective.ULTRASHORT, today);
					
					// If the stock is currently not held, BUY it if the BUY signal is given and the stock is in a DOWNTREND
					if (byingPrice == 0.0f && trend == Trend.DOWN) {
						
						byingPrice = priceHist.get(today).getClosingPrice();

						log4j.debug("We got a Island BUY signal for " + stock.getCompanyName() + 
								"at date: " + priceHist.get(islandDay).getShortDateString());
					}
				}

				if (priceHist.get(yesterday).getHighestPrice() < priceHist.get(islandDay).getLowestPrice() &&
					priceHist.get(today).getHighestPrice() < priceHist.get(islandDay).getLowestPrice()) {
					
					Trend trend = TrendCalculator.simpleTrend(stock, Perspective.ULTRASHORT, today);
					
					if (byingPrice != 0.0f && trend == Trend.UP) {
						totalWinnings += priceHist.get(today).getClosingPrice() - byingPrice;
						byingPrice = 0.0f;
						
						log4j.debug("We got a Island SELL signal for " + stock.getCompanyName() + 
								"at date: " + priceHist.get(islandDay).getShortDateString());
					}
				}
				// If the initial stoploss triggers, sell the stock
				if (byingPrice != 0.0f && priceHist.get(today).getClosingPrice() < (byingPrice * (1 - ((float)argInitStopLoss / 100))) ) {
					
					totalWinnings += priceHist.get(today).getClosingPrice() - byingPrice;

					log4j.debug("Stop loss triggered for " + stock.getCompanyName() + " at date: " + 
								priceHist.get(islandDay).getShortDateString() +  ", last buy/sell result: " + 
								byingPrice + " - " + priceHist.get(today).getClosingPrice() + " = " + 
								(priceHist.get(today).getClosingPrice() - byingPrice) );

					byingPrice = 0.0f;
				}
				yesterday++;
				islandDay++;
				today++;
			}
			log4j.info("Total winning price for " + stock.getCompanyName() + ": " + totalWinnings);
		}
	} // end backTrade()

	
} // end class StockFilter
