package com.stocktastic.filter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.filter.TrendCalculator.Trend;
import com.stocktastic.pojo.Stock;

/**
* This is a filter to take a chance and then rely on stop loss being enforced.
* First, an initial stop loss i set to specified percent value. If break even
* is passed, a trailing stop loss replaces the initial stop loss with the intent
* to act as a guard against losses.
*
* @author stationaradm
*/
public class StopLossChance extends StockFilter {

	private final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	private static Logger log4j;
	// Represents the initial stop loss for a position. Used for backtrading
	private int initialStopLoss;
	// Represents the trailing stop loss for a position. Used for backtrading
	private int trailStopLoss;
	
	// A float array holding moving average values for the price history
	private float[] maValues;
	
	// The selected moving average period in days.
	private int maPeriod = 200;
	
	/**
	 * Constructor for class StopLossChance without stop loss level arguments. This is used during the full backtrade
	 * where different stop loss levels are tried for statistics.
	 */
	public StopLossChance() {
		super("Stop Loss Chance", "Buy the stock randomly and then use stop loss to manage the trade.");
		log4j = LogManager.getLogger(StopLossChance.class.getName());
	} // end constructor StopLossChance
	
	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		log4j.info("The random start of the stop loss triggered trade is not applicable for daily filter, use -b backtrade instead.");
	} // end applyFilter()
	
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		initialStopLoss = argInitStopLoss;
		trailStopLoss = argTrailStopLoss;
		
		stock = argStock;
		priceHist = stock.getPrices();
		float[] maValues = StockTasticUtils.calcMaValues(priceHist, maPeriod);
		
		float byingPrice = 0.0f;
		float totalWinnings = 0.0f;

		int idx = 0;

		if (argStartDate != null) {
			idx = checkStartDate(argStartDate);
		}

		// Make sure we have at least the number of price points in history as the long
		// moving average in order to be able to calculate a valid MA value.
		if (priceHist.size() > 0 && idx > -1) {
			
			// The price level that will trigger a trailing stop loss.
			float trailStopLossPrice = 0.0f;
			// The price level when the trailing stop will be raised.
			float trailStopLossLevelRaisePrice = 0.0f;
			
			while (idx < priceHist.size()) {
				
				// Randomly decide whether to buy the stock at this position in time or not.
				double randVal = Math.random();
				boolean buyRandomDecision = randVal > 0.5 ? true : false;
				
				// Buy the stock if randomly decided so and the stock is not currently held.
				if (buyRandomDecision && byingPrice == 0) {
					
					// Check that the trend for chosen MA value is rising before bying.
					Trend currentTrend = TrendCalculator.calcMaTrend(stock, maValues, Perspective.SHORT, idx, maPeriod);
					
					if (currentTrend == Trend.UP) {
						byingPrice = priceHist.get(idx).getClosingPrice();
						log4j.debug("Buy the stock: {}, at date: {} with closing price: {}", stock.getCompanyName(), 
								    priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());
					}
					else {
						log4j.debug("Randomly selected to buy stock but trend was down or sideways so aborting... ({},{},{})", 
								    stock.getCompanyName(), priceHist.get(idx).getShortDateString(), 
								    priceHist.get(idx).getClosingPrice());
					}
				}

				// If the stock is held and the initial stoploss level is reached at closing price, sell the stock
				if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
					priceHist.get(idx).getClosingPrice() < (byingPrice * (1 - ((float)initialStopLoss / 100))) ) {
					
					totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

					log4j.debug("Initial stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(idx).getClosingPrice(), 
								byingPrice, (priceHist.get(idx).getLowestPrice() - byingPrice));
					log4j.debug("Initial stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
							     priceHist.get(idx).getClosingPrice(), priceHist.get(idx).getShortDateString());

					byingPrice = 0.0f;
				}

				// Initiate the trailing stop loss If the price reaches a break even (initial price + stopinitial stoploss.
				if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
					priceHist.get(idx).getClosingPrice() > (byingPrice * (1 + ((float)initialStopLoss / 100))) ) {

					// Set the current trailing stop loss level.
					trailStopLossPrice = priceHist.get(idx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
					// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
					// TODO Evaluate how the raising of this level can be optimised.
					trailStopLossLevelRaisePrice = priceHist.get(idx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));
					
					log4j.debug("Trailing stop loss initiated for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
							    priceHist.get(idx).getClosingPrice(), priceHist.get(idx).getShortDateString());
				}

				// Increase the trailing stop loss if the price increases with the initial stop loss percentage again.
				if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && 
					priceHist.get(idx).getClosingPrice() > trailStopLossLevelRaisePrice) {

					// Set the current trailing stop loss level.
					trailStopLossPrice = priceHist.get(idx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
					// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
					// TODO Evaluate how the raising of this level can be optimised.
					trailStopLossLevelRaisePrice = priceHist.get(idx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));
					
					log4j.debug("Trailing stop loss raised for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
							    priceHist.get(idx).getClosingPrice(), priceHist.get(idx).getShortDateString());
				}

				// If the price reaches the trailing stop loss trigger a sell.
				if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && (priceHist.get(idx).getClosingPrice() < trailStopLossPrice) ) {

					totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

					log4j.debug("Trailing stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(idx).getClosingPrice(), 
								byingPrice, (priceHist.get(idx).getClosingPrice() - byingPrice));
					log4j.debug("Trailing stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
							    priceHist.get(idx).getClosingPrice(), priceHist.get(idx).getShortDateString());

					byingPrice = 0.0f;
					trailStopLossPrice = 0.0f;
					trailStopLossLevelRaisePrice = 0.0f;
				}

				idx++;
			}
			log4j.info("Total winning price for " + stock.getCompanyName() + ": " + totalWinnings);	
		}				
	} // end backTrade()
	
	public void fullBackTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		initialStopLoss = argInitStopLoss;
		trailStopLoss = argTrailStopLoss;
		
		stock = argStock;
		priceHist = stock.getPrices();
		float[] maValues = StockTasticUtils.calcMaValues(priceHist, maPeriod);
		ArrayList<FilterResult> results = new ArrayList<FilterResult>();
		int idx = 0;
		
		if (argStartDate != null) {
			idx = checkStartDate(argStartDate);
		}
		else {
			argStartDate = new GregorianCalendar();
		}
		
		// Make sure we have at least the number of price points in history as the long
		// moving average in order to be able to calculate a valid MA value.
		if (priceHist.size() > 0 && idx > -1) {

			// Loop through all levels of initial stop losses (1 to 5 %)
			for (int initialStopLoss = 1; initialStopLoss < 6; initialStopLoss++) {

				// For each level of initial stop loss, loop through all levels of trailing stop loss (1 to 5 %)
				for (int trailStopLoss = 1; trailStopLoss < 6; trailStopLoss++) {

					// This is the offset from the start of the price history. For each combination
					// of initial and trailing stop loss, the backtrading starts from the first available 
					// date in the price history and after running through the complete price history, start 
					// from the second oldest price history date and repeat until the last avaiable date has 
					// been reached in the price history.
					int offset = 0;

					//Statistics for this combination of initial and trailing stop loss
					float worstResult = 0.0f;
					float bestResult = 0.0f;
					Date worstResultStartDate = argStartDate.getTime();
					Date bestResultStartDate = argStartDate.getTime();

					// Sum for medium value
					float totalSum = 0.0f;
					// Divisor for the medium value.
					int numIterations = 0;
					
					// For each combination of initial and trailing stop loss e.g. 1,1 1,2 ,1,3 2,1, 2,2 2,3 3,1, 3,2 3,3
					// Loop through the complete price history and start at each day trying out the combination for the complete
					// price history from start data idx + offset to today
					while ( (idx + offset) < priceHist.size() ) {
						
						// The current bying price for the stock
						float byingPrice = 0.0f;
						// The total winnings from the filter.
						float totalWinnings = 0.0f;
						// The price that will trigger a trailing stop loss.
						float trailStopLossPrice = 0.0f;
						// The price level when the trailing stop will be raised.
						float trailStopLossLevelRaisePrice = 0.0f;
						
						// Apply the filter to the specific combination of inital and traling stop losses at from this specific start date.
						for (int innerIdx = idx + offset; innerIdx < priceHist.size(); innerIdx++) {
							
							double randVal = Math.random();
							boolean buyRandomDecision = randVal > 0.5 ? true : false;
							
							// Buy the stock randomly if it is not held currently and we are in an uptrend.
							if (byingPrice == 0 && buyRandomDecision) {
								
								// Check that the trend for chosen MA value is rising.
								Trend currentTrend = TrendCalculator.calcMaTrend(stock, maValues, Perspective.SHORT, innerIdx, maPeriod);
								
								if (currentTrend == Trend.UP) {
									byingPrice = priceHist.get(innerIdx).getClosingPrice();
									log4j.debug("Buy the stock: {}, at date: {} with closing price: {}", stock.getCompanyName(), 
											    priceHist.get(innerIdx).getShortDateString(), priceHist.get(innerIdx).getClosingPrice());
								}
								else {
									log4j.debug("Took a chance to buy stock but trend was down or sideways so aborting... ({},{},{})", 
											    stock.getCompanyName(), priceHist.get(innerIdx).getShortDateString(), 
											    priceHist.get(innerIdx).getClosingPrice());
								}
							}

							// If the initial stoploss triggers, sell the stock
							if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() < (byingPrice * (1 - ((float)initialStopLoss / 100))) ) {
								
								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;

								log4j.debug("Initial stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(innerIdx).getClosingPrice(), 
											byingPrice, (priceHist.get(innerIdx).getLowestPrice() - byingPrice));
								log4j.debug("Initial stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
										     priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());

								byingPrice = 0.0f;
							}

							// Initiate the trailing stop loss If the price reaches a break even (initial price + stopinitial stoploss.
							if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() > (byingPrice * (1 + ((float)initialStopLoss / 100))) ) {

								// Set the current trailing stop loss level.
								trailStopLossPrice = priceHist.get(innerIdx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
								// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
								// TODO Evaluate how the raising of this level can be optimised.
								trailStopLossLevelRaisePrice = priceHist.get(innerIdx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));

								log4j.debug("Trailing stop loss initiated for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());
							}

							// Increase the trailing stop loss if the price increases with the initial stop loss percentage again.
							if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() > (trailStopLossPrice * (1 + ((float)initialStopLoss / 100))) ) {

								// Set the current trailing stop loss level.
								trailStopLossPrice = priceHist.get(innerIdx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
								// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
								// TODO Evaluate how the raising of this level can be optimised.
								trailStopLossLevelRaisePrice = priceHist.get(innerIdx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));
								
								log4j.debug("Trailing stop loss raised for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());
							}

							// If the price reaches the trailing stop loss trigger a sell.
							if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && (priceHist.get(innerIdx).getClosingPrice() < trailStopLossPrice) ) {

								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;

								log4j.debug("Trailing stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(innerIdx).getClosingPrice(), 
											byingPrice, (priceHist.get(innerIdx).getClosingPrice() - byingPrice));
								log4j.debug("Trailing stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());

								byingPrice = 0.0f;
								trailStopLossPrice = 0.0f;
								trailStopLossLevelRaisePrice = 0.0f;
							}
						}
						
						if (worstResult > totalWinnings) {
							worstResult = totalWinnings;
							worstResultStartDate = priceHist.get(idx + offset).getDate();
						}
						if (bestResult < totalWinnings) {
							bestResult = totalWinnings;
							bestResultStartDate = priceHist.get(idx + offset).getDate();
						}
						
						// Add to the variables for medium calculation for the current start date
						totalSum += totalWinnings;
						numIterations += 1;
						
						log4j.debug("Total winning price starting from " + priceHist.get(idx + offset).getShortDateString() + " for " + stock.getCompanyName() + 
									": " + totalWinnings);	
						offset += 1;
					}
					
					//Calculate the medium winnings/loss from this iteration
					float mediumResult = totalSum / numIterations;
					
					// Add the results from this iteration to the total results
					FilterResult isIt = new FilterResult(initialStopLoss, trailStopLoss, bestResult, worstResult, mediumResult, bestResultStartDate, worstResultStartDate);
					results.add(isIt);
				}
			}
			
			log4j.info( "Result of full backtrade for filter {} on stock {}", getFilterName(), stock.getCompanyName());
			log4j.info( "---------------------------------------------------------------------------------------");
			log4j.info( "Initial Trailing Best Date	Best	Worst Date	Worst	Medium");
			log4j.info( "---------------------------------------------------------------------------------------");
			
			for (FilterResult result : results) {
				log4j.info(result.toString());
			}
			log4j.info( "---------------------------------------------------------------------------------------");
			
		}
	} // end fullBackTrade()
	
	public void latestSignal(Stock stock) {} // end latestSignal()
	
} // end class StockFilter
