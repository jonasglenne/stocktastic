package com.stocktastic.filter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 *  Inner Class to handle results from a filter run with the 
 */
public class FilterResult {
	
	int initialStopLoss;
	int trailingStopLoss;
	float bestResult;
	float worstResult;
	float mediumResult;
	Date bestResultDate;
	Date worstResultDate;
	
	private final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	private final static DecimalFormat decFormat = new DecimalFormat();
	/**
	 * Constructor class FilterResult
	 * @param argInitialStopLoss an int that is the intial stop loss level representing percent
	 * @param argTrailingStopLoss an int that is the trailing stop loss level representing percent
	 * @param argBestResult a float that is the best result produced from the stop loss combination 
	 * @param argWorstResult a float that is the worst result produced from the stop loss combination 
	 * @param argMediumResult  a float that is the medium result produced from the stop loss combination 
	 * @param argBestResultDate a Date that is the start date for the best result produced from the stop loss combination 
	 * @param argWorstResultDate a Date that is the start date for the worst result produced from the stop loss combination 
	 */
	
	public FilterResult(int argInitialStopLoss, int argTrailingStopLoss, float argBestResult, 
						float argWorstResult, float argMediumResult, Date argBestResultDate,
						Date argWorstResultDate) {
		initialStopLoss = argInitialStopLoss;
		trailingStopLoss = argTrailingStopLoss;
		bestResult = argBestResult;
		worstResult = argWorstResult;
		mediumResult = argMediumResult;
		bestResultDate = argBestResultDate;
		worstResultDate = argWorstResultDate;
	} // end constructor FilterResult()
	
	public String toString() {
		decFormat.setMaximumFractionDigits(3);
		return initialStopLoss + "	" + trailingStopLoss + "	 " + df.format(bestResultDate) + "	" + decFormat.format(bestResult) + 
				"	" + df.format(worstResultDate) + "	" + decFormat.format(worstResult) + "	" + decFormat.format(mediumResult); 
	} // end toString()
	
} // end class FilterReult
