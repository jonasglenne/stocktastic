package com.stocktastic.filter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.filter.TrendCalculator.Trend;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;

/**
* The key reversal does not occur very often but is very reliable when it does.
*
* After an up-trend (sell signal):
*
*    The Open must be above yesterday's Close,
*    The day must make a new High, and
*    The Close must be below yesterday's Low.
*
* After a down-trend (buy signal):
*
*    The Open must be below yesterday's Close,
*    The day must make a new Low, and
*    The Close must be above yesterday's High.
*/


//TODO Take trend into account for key reversal day.
/**
 * Abstract class that represents a filter. Contains the filter name
 * and basic functionality. Override applyFilter() to define behavior
 * 
 * @author stationaradm
 */
public class KeyReversal extends StockFilter {
	
	private static Logger log4j;

	// Represents the initial stop loss for a position. Used for backtrading
	private int initialStopLoss;
	// Represents the trailing stop loss for a position. Used for backtrading
	private int trailStopLoss;
	
	/**
	 * Constructor for class KeyReversal with only the name
	 * of the filter as a parameter.
	 * 
	 * @param shortMa an Integer that represents the number of days to calculate the moving average value
	 * @param longMaPeriod an Integer that represents the number of days to calculate the moving average value
	 */
	public KeyReversal() {
		super("Key Reversal", "Buy when the stock has been traded below the lowest price yesterday but closes above the highest price yesterday.");
		log4j = LogManager.getLogger(KeyReversal.class.getName());
	} // end constructor MAFilter

	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		Date currentDate = StockTasticUtils.getCurrentDate();	
		
		// Make sure we have at least the number of price points in history as the long
		// moving average in order to be able to calculate a valid MA value.
		if (priceHist.size() > 2) {
			
			int idx = priceHist.size() -1;

			do {
				
				long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(idx).getDate());

				float todayOpen = priceHist.get(idx).getOpeningPrice();
				float todayLow = priceHist.get(idx).getLowestPrice();
				float todayHigh = priceHist.get(idx).getHighestPrice();
				float todayClose = priceHist.get(idx).getClosingPrice();
				
				float yesterdayLow = priceHist.get(idx -1).getLowestPrice();
				float yesterdayClose = priceHist.get(idx -1).getClosingPrice();
				float yesterdayHigh = priceHist.get(idx -1).getHighestPrice();
				
				if (todayOpen < yesterdayClose && todayLow < yesterdayLow && todayClose > yesterdayHigh) {

					Trend trend = TrendCalculator.simpleTrend(stock, Perspective.SHORT, idx);

					// To be a valid signal, it has to reverse an opposite trend (i.e. trend should be down for a positive key reversal to be valid).
					if (trend == Trend.DOWN) {
						
						stock.addSignal(new Signal(getFilterName(), 
								priceHist.get(idx).getDate(), 
								differenceInDays + " day(s) old Key Reversal BUY signal for " + stock.getCompanyName() + 
								" at date: " + priceHist.get(idx).getShortDateString() + " with closing price:" + 
								priceHist.get(idx).getClosingPrice(),
								SignalType.BUY));
						
						log4j.info("{} day(s) old. Key Reversal BUY signal for {} at date: {}", differenceInDays, 
								    stock.getCompanyName(), priceHist.get(idx).getShortDateString());

						continueUntilSignalFound = false;
					}
					// If the reversal is not opposite to trend note it down at least until the algorithm is good enough.
					else {
						stock.addSignal(new Signal(getFilterName(), 
								priceHist.get(idx).getDate(), 
								differenceInDays + " day(s) old Key Reversal BUY signal for " + stock.getCompanyName() + 
								" (but not against trend) at date: " + priceHist.get(idx).getShortDateString() + " with closing price:" + 
								priceHist.get(idx).getClosingPrice(),
								SignalType.BUY));
						
						log4j.debug("{} day(s) old. Key Reversal SELL signal (but not against trend) for {} at date: {}", differenceInDays, 
							    stock.getCompanyName(), priceHist.get(idx).getShortDateString());
					}
				}

				if (todayOpen > yesterdayClose && todayHigh > yesterdayHigh && todayClose < yesterdayLow) {

					Trend trend = TrendCalculator.simpleTrend(stock, Perspective.SHORT, idx);

					// To be a valid signal, it has to reverse an opposite trend (i.e. trend should be up for 
					// a negative key reversal to be valid).
					if (trend == Trend.UP) {

						stock.addSignal(new Signal(getFilterName(), 
								priceHist.get(idx).getDate(), 
								differenceInDays + " day(s) old. Key Reversal SELL signal for " + stock.getCompanyName() + 
								" at date: " + priceHist.get(idx).getShortDateString() + " with closing price:" + 
								priceHist.get(idx).getClosingPrice(),
								SignalType.SELL));

						log4j.info("{} day(s) old. Key Reversal SELL signal for {} at date: {}", differenceInDays, 
							    stock.getCompanyName(), priceHist.get(idx).getShortDateString());

						continueUntilSignalFound = false;
					}
					// If the reversal is not opposite to trend note it down at least until the algorithm is good enough.
					else {
						stock.addSignal(new Signal(getFilterName(), 
								priceHist.get(idx).getDate(), 
								differenceInDays + " day(s) old. Key Reversal SELL signal for " + stock.getCompanyName() + 
								" (but not against trend) at date: " + priceHist.get(idx).getShortDateString() + 
								" with closing price:" + priceHist.get(idx).getClosingPrice(), SignalType.SELL));

						log4j.debug("{} day(s) old. Key Reversal SELL signal (but not against trend) for {} at date: {}", 
									differenceInDays, stock.getCompanyName(), priceHist.get(idx).getShortDateString());
					}
				}

				log4j.debug("todayOpen was {}", todayLow);
				log4j.debug("todayHigh was {}", todayHigh);
				log4j.debug("todayLow was {}", todayLow);
				log4j.debug("todayClose was {}", todayClose);
				log4j.debug("yesterdayLow was {}", yesterdayLow);
				log4j.debug("yesterdayClose was {}", yesterdayClose);
				log4j.debug("yesterdayHigh was {}", yesterdayHigh);

				idx--;
			} while (continueUntilSignalFound == true && idx > 1);
		}
		else {
			log4j.info("Not enough price history to calculate key reversal,  required: {}, found: {}", 2 , (priceHist.size() -1));
		}
	} // end applyFilter()


	@Override
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		
		float byingPrice = 0.0f;
		float totalWinnings = 0.0f;
		
		int idx = 1;
		
		if (argStartDate != null) {
			idx = checkStartDate(argStartDate) + 1;
		}

		// Make sure we have at least the number of price points in history as the long
		// moving average in order to be able to calculate a valid MA value.
		if (priceHist.size() > 2 && idx > -1) {
			
			while (idx < priceHist.size()) {
				
				float todayOpen = priceHist.get(idx).getOpeningPrice();
				float todayLow = priceHist.get(idx).getLowestPrice();
				float todayHigh = priceHist.get(idx).getHighestPrice();
				float todayClose = priceHist.get(idx).getClosingPrice();
				
				float yesterdayLow = priceHist.get(idx -1).getLowestPrice();
				float yesterdayClose = priceHist.get(idx -1).getClosingPrice();
				float yesterdayHigh = priceHist.get(idx -1).getHighestPrice();
				
				if (todayOpen < yesterdayClose && todayLow < yesterdayLow && todayClose > yesterdayHigh) {
					
					Trend trend = TrendCalculator.simpleTrend(stock, Perspective.SHORT, idx);
					
					// If the stock is currently not held, BUY it if the BUY signal is given and the stock is in a DOWNTREND
					if (byingPrice == 0.0f && trend == Trend.DOWN) {
						byingPrice = priceHist.get(idx).getHighestPrice();
						log4j.debug("Key Reversal BUY signal for {}, at date: {}", stock.getCompanyName(), 
									priceHist.get(idx).getShortDateString());
					}
				}

				if (todayOpen > yesterdayClose && todayHigh > yesterdayHigh && todayClose < yesterdayLow) {

					Trend trend = TrendCalculator.simpleTrend(stock, Perspective.SHORT, idx);
					
					// If the stock is currently held (have been bought, SELL it if the SELL signal is given and the stock is in uptrend.
					if (byingPrice != 0.0f && trend == Trend.UP) {
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;
						byingPrice = 0.0f;
						
						log4j.debug("Key Reversal SELL signal for {}, at date: {}", stock.getCompanyName(), 
									priceHist.get(idx).getShortDateString());
					}
				}
				// If the initial stoploss triggers, sell the stock
				if (byingPrice != 0.0f && priceHist.get(idx).getClosingPrice() < (byingPrice * (1 - ((float)argInitStopLoss / 100))) ) {
					
					totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

					log4j.debug("Stop loss triggered SELL, last buy/sell result: {} - {} = {}", byingPrice,priceHist.get(idx).getClosingPrice(),
							   (priceHist.get(idx).getClosingPrice() - byingPrice));
					log4j.debug("Stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
							    priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());

					byingPrice = 0.0f;
				}

				log4j.trace("todayOpen was {}", todayLow);
				log4j.trace("todayHigh was {}", todayHigh);
				log4j.trace("todayLow was {}", todayLow);
				log4j.trace("todayClose was {}", todayClose);
				log4j.trace("yesterdayLow was {}", yesterdayLow);
				log4j.trace("yesterdayClose was {}", yesterdayClose);
				log4j.trace("yesterdayHigh was {}", yesterdayHigh);

				idx++;
			}
			log4j.info("Total winning price for " + stock.getCompanyName() + ": " + totalWinnings);
		}
	} // end backTrade()
	
	
	public void fullBackTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		
		initialStopLoss = argInitStopLoss;
		trailStopLoss = argTrailStopLoss;
		
		stock = argStock;
		priceHist = stock.getPrices();

		int startDateIdx = 1;
		
		if (argStartDate != null) {
			startDateIdx = checkStartDate(argStartDate) + 1;
		}

		// Make sure we have at least the number of price points in history as the long
		// moving average in order to be able to calculate a valid MA value.
		if (priceHist.size() > 2 && startDateIdx > -1) {
			
			ArrayList<FilterResult> results = new ArrayList<FilterResult>();

			// Loop through all levels of initial stop losses (1 to 5 %)
			for (int initialStopLoss = 1; initialStopLoss < 6; initialStopLoss++) {
			
				// For each level of initial stop loss, loop through all levels of trailing stop loss (1 to 5 %)
				for (int trailStopLoss = 1; trailStopLoss < 6; trailStopLoss++) {
					
					// This is the offset from the start of the price history. For each combination
					// of initial and trailing stop loss, the backtrading starts from the first available 
					// date in the price history and after running through the complete price history, start 
					// from the second oldest price history date and repeat until the last avaiable date has 
					// been reached in the price history.
					int offset = 0;

					//Statistics for this combination of initial and trailing stop loss
					float worstResult = 0.0f;
					float bestResult = 0.0f;
					Date worstResultStartDate = argStartDate.getTime();
					Date bestResultStartDate = argStartDate.getTime();

					// Sum for medium value
					float totalSum = 0.0f;
					// Divisor for the medium value.
					int numIterations = 0;
					
					// For each combination of initial and trailing stop loss e.g. 1,1 1,2 ,1,3 2,1, 2,2 2,3 3,1, 3,2 3,3
					// Loop through the complete price history and start at each day trying out the combination for the complete
					// price history from start data idx + offset to today
					while ( (startDateIdx + offset) < priceHist.size() ) {
						
						// The current bying price for the stock
						float byingPrice = 0.0f;
						// The total winnings from the filter.
						float totalWinnings = 0.0f;
						// The price that will trigger a trailing stop loss.
						float trailStopLossPrice = 0.0f;
						// The price level when the trailing stop will be raised.
						float trailStopLossLevelRaisePrice = 0.0f;
						
						// Apply the filter to the specific combination of inital and traling stop losses at from this specific start date.
						for (int innerIdx = startDateIdx + offset; innerIdx < priceHist.size(); innerIdx++) {
							
							float todayOpen = priceHist.get(innerIdx).getOpeningPrice();
							float todayLow = priceHist.get(innerIdx).getLowestPrice();
							float todayHigh = priceHist.get(innerIdx).getHighestPrice();
							float todayClose = priceHist.get(innerIdx).getClosingPrice();
							
							float yesterdayLow = priceHist.get(innerIdx -1).getLowestPrice();
							float yesterdayClose = priceHist.get(innerIdx -1).getClosingPrice();
							float yesterdayHigh = priceHist.get(innerIdx -1).getHighestPrice();
							
							//=====================================================================================================
							// Key reversal rules
							//=====================================================================================================

							if (todayOpen < yesterdayClose && todayLow < yesterdayLow && todayClose > yesterdayHigh) {
								
								Trend trend = TrendCalculator.simpleTrend(stock, Perspective.SHORT, innerIdx);
								
								// If the stock is currently not held, BUY it if the BUY signal is given and the stock is in a DOWNTREND
								if (byingPrice == 0.0f && trend == Trend.DOWN) {
									byingPrice = priceHist.get(innerIdx).getHighestPrice();
									log4j.debug("Key Reversal BUY signal for {}, at date: {}", stock.getCompanyName(), 
												priceHist.get(innerIdx).getShortDateString());
								}
							}
							if (todayOpen > yesterdayClose && todayHigh > yesterdayHigh && todayClose < yesterdayLow) {

								Trend trend = TrendCalculator.simpleTrend(stock, Perspective.SHORT, innerIdx);
								
								// If the stock is currently held (have been bought, SELL it if the SELL signal is given and the stock is in uptrend.
								if (byingPrice != 0.0f && trend == Trend.UP) {
									totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;
									byingPrice = 0.0f;
									
									log4j.debug("Key Reversal SELL signal for {}, at date: {}", stock.getCompanyName(), 
												priceHist.get(innerIdx).getShortDateString());
								}
							}

							log4j.trace("todayOpen was {}", todayLow);
							log4j.trace("todayHigh was {}", todayHigh);
							log4j.trace("todayLow was {}", todayLow);
							log4j.trace("todayClose was {}", todayClose);
							log4j.trace("yesterdayLow was {}", yesterdayLow);
							log4j.trace("yesterdayClose was {}", yesterdayClose);
							log4j.trace("yesterdayHigh was {}", yesterdayHigh);
							
							//=====================================================================================================
							// Stop loss rules
							//=====================================================================================================

							// If the initial stoploss triggers, sell the stock
							if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() < (byingPrice * (1 - ((float)initialStopLoss / 100))) ) {
								
								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;

								log4j.debug("Initial stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(innerIdx).getClosingPrice(), 
											byingPrice, (priceHist.get(innerIdx).getLowestPrice() - byingPrice));
								log4j.debug("Initial stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
										     priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());

								byingPrice = 0.0f;
								trailStopLossPrice = 0.0f;
								trailStopLossLevelRaisePrice = 0.0f;
							}
							
							// Initiate the trailing stop loss If the price reaches a break even (initial price + stopinitial stoploss.
							if (byingPrice != 0.0f && trailStopLossPrice == 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() > (byingPrice * (1 + ((float)initialStopLoss / 100))) ) {

								// Set the current trailing stop loss level.
								trailStopLossPrice = priceHist.get(innerIdx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
								// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
								// TODO Evaluate how the raising of this level can be optimised.
								trailStopLossLevelRaisePrice = priceHist.get(innerIdx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));

								log4j.debug("Trailing stop loss initiated for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());
							}
							
							// Increase the trailing stop loss if the price increases with the initial stop loss percentage again.
							if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && 
								priceHist.get(innerIdx).getClosingPrice() > trailStopLossLevelRaisePrice ) {

								// Set the current trailing stop loss level.
								trailStopLossPrice = priceHist.get(innerIdx).getClosingPrice() - (1 - ((float)trailStopLoss / 100));
								// Increase the price level of the next trailing stop loss raise to be today's closing price + initial stop loss level.
								// TODO Evaluate how the raising of this level can be optimised.
								trailStopLossLevelRaisePrice = priceHist.get(innerIdx).getClosingPrice() * (1 + ((float)initialStopLoss / 100));
								
								log4j.debug("Trailing stop loss raised for: {} at closing price: {}, at date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());
							}
							
							// If the price reaches the trailing stop loss trigger a sell.
							if (byingPrice != 0.0f && trailStopLossPrice != 0.0f && (priceHist.get(innerIdx).getClosingPrice() < trailStopLossPrice) ) {

								totalWinnings += priceHist.get(innerIdx).getClosingPrice() - byingPrice;

								log4j.debug("Trailing stop loss triggered SELL, last buy/sell result: {} - {} = {}", priceHist.get(innerIdx).getClosingPrice(), 
											byingPrice, (priceHist.get(innerIdx).getClosingPrice() - byingPrice));
								log4j.debug("Trailing stop loss triggered for {}, closing price: {}, date: {}", stock.getCompanyName(), 
										    priceHist.get(innerIdx).getClosingPrice(), priceHist.get(innerIdx).getShortDateString());

								byingPrice = 0.0f;
								trailStopLossPrice = 0.0f;
								trailStopLossLevelRaisePrice = 0.0f;
							}
						}
						
						if (worstResult > totalWinnings) {
							worstResult = totalWinnings;
							worstResultStartDate = priceHist.get(startDateIdx + offset).getDate();
						}
						if (bestResult < totalWinnings) {
							bestResult = totalWinnings;
							bestResultStartDate = priceHist.get(startDateIdx + offset).getDate();
						}
						
						// Add to the variables for medium calculation for the current start date
						totalSum += totalWinnings;
						numIterations += 1;
						
						log4j.debug("Total winning price starting from " + priceHist.get(startDateIdx + offset).getShortDateString() + " for " + stock.getCompanyName() + 
									": " + totalWinnings);	
						offset += 1;
					}
					
					//Calculate the medium winnings/loss from this iteration
					float mediumResult = totalSum / numIterations;
					
					// Add the results from this iteration to the total results
					FilterResult isIt = new FilterResult(initialStopLoss, trailStopLoss, bestResult, worstResult, mediumResult, bestResultStartDate, worstResultStartDate);
					results.add(isIt);

				}
			}
			
			log4j.info( "Result of full backtrade for filter {} on stock {}", getFilterName(), stock.getCompanyName());
			log4j.info( "---------------------------------------------------------------------------------------");
			log4j.info( "Initial Trailing Best Date	Best	Worst Date	Worst	Medium");
			log4j.info( "---------------------------------------------------------------------------------------");
			
			for (FilterResult result : results) {
				log4j.info(result.toString());
			}
			log4j.info( "---------------------------------------------------------------------------------------");
		}
	} // end fullBackTrade()
	
	public void latestSignal(Stock stock) {} // end latestSignal()
	
} // end class StockFilter
