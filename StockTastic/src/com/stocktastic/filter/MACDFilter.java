package com.stocktastic.filter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.StockTasticUtils;
import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;


/**
 * Formula is: EMA = Price(t) * k + EMA(y) * (1 – k) where EMA(y) is yestedays EMA.
 * k = 2 / (n-day period + 1)
 * EMA(Y) needs to be calculated a first time using single moving average (because it
 * has to start somewhere)
 * 
 * Go long when MACD crosses its signal line from below.
 * Go short when MACD crosses its signal line from above.
 *
 * Signals are far stronger if there is either:
 * a divergence on the MACD indicator; or
 * a large swing above or below the zero line.
 * 
 * @author stationaradm
 */
public class MACDFilter extends StockFilter {
	
	// The length in days of the slow EMA
	private int slowEMALength;
	// The length in days of the fast EMA
	private int fastEMALength;
	// The length in days for the signal line of the the MACD
	private int signalLineLength;
	// Holds all the MACD values for the price history. 
	private float[] macdValues;
	// Holds all signal line values (n days EMA of the MACD values)
	private float[] signalLineValues;
	// Price history for the the stock which to apply this filter.
	private ArrayList<PriceListing> priceHist;
	
	private static Logger log4j;
	
	/**
	 * Constructor for class MACDFilter
	 * 
	 * @param slowEMALength an integer that is the number of days for the slow EMA
	 * @param fastEMALength an integer that is the number of days for the fast EMA
	 * @param signalLineLength an integer that is the number of days for the signal line
	 */
	public MACDFilter(int slowEMALength, int fastEMALength, int signalLineLength) {
		super("MACD", "Distance between two different moving averages 21 (slow), 12 (fast) and signal line 9. When MACD");
		
		this.slowEMALength = slowEMALength;
		this.fastEMALength = fastEMALength;
		this.signalLineLength = signalLineLength;
		
		log4j = LogManager.getLogger(MACDFilter.class.getName());
	} // end constructor MACDFilter()

	@Override
	public void applyFilter(Stock argStock, boolean continueUntilSignalFound) {
		
		stock = argStock;
		priceHist = stock.getPrices();
		Date currentDate = StockTasticUtils.getCurrentDate();	

		// Make first sure we have data for at least one calculation of MACD.
		// Add + 1 to longEMA since we have to be able to calculate yesterdays EMA, 
		// i.e. EMA(y) in the formula. Formula is: 
		// EMA = Price(t) * k + EMA(y) * (1 – k) where EMA(y) is yestedays EMA.
		if (priceHist.size() > (slowEMALength + 1) ) {

			boolean signalFound = false;

			macdValues = calcMACDValues();
			signalLineValues = calcSignalLine();
			
			int priceHistPosition = priceHist.size() -1;
			
			do {

				long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, priceHist.get(priceHistPosition).getDate());

				// Bullish signal line crossover
				if (macdValues[priceHistPosition] > signalLineValues[priceHistPosition] &&
					macdValues[priceHistPosition -1] <= signalLineValues[priceHistPosition -1]) {

					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(priceHistPosition).getDate(), 
							differenceInDays + " day(s) old MACD Signal Crossover BUY signal for " + stock.getCompanyName() + 
							" at date: " + priceHist.get(priceHistPosition).getShortDateString() + " with closing price:" + 
							priceHist.get(priceHistPosition).getClosingPrice(),
							SignalType.BUY));
					
					log4j.debug("{} day(s) old. MACD Signal Crossover BUY signal for {} at date: {}", differenceInDays, 
								stock.getCompanyName(), priceHist.get(priceHistPosition).getShortDateString());
					signalFound = true;
				}
				// Bearish signal line crossover
				else if (macdValues[priceHistPosition] < signalLineValues[priceHistPosition] &&
						macdValues[priceHistPosition -1] >= signalLineValues[priceHistPosition -1] ) {

					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(priceHistPosition).getDate(), 
							differenceInDays + " day(s) old MACD Signal Crossover SELL signal for " + stock.getCompanyName() + 
							" at date: " + priceHist.get(priceHistPosition).getShortDateString() + " with closing price:" + 
							priceHist.get(priceHistPosition).getClosingPrice(),
							SignalType.SELL));
					

					log4j.debug("{} day(s) old. MACD Signal Crossover SELL signal for {} at date: {}", differenceInDays, 
								stock.getCompanyName(), priceHist.get(priceHistPosition).getShortDateString());
					signalFound = true;
				}
				
				// Bullish zero line crossover
				if (macdValues[priceHistPosition] > 0 && macdValues[priceHistPosition -1] <= 0) {

					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(priceHistPosition).getDate(), 
							differenceInDays + " day(s) old MACD zero line crossover BUY signal for " + stock.getCompanyName() + 
							" at date: " + priceHist.get(priceHistPosition).getDate() + " with closing price:" + 
							priceHist.get(priceHistPosition).getClosingPrice(),
							SignalType.BUY));
					
					log4j.debug("{} day(s) old. MACD zero line crossover BUY signal for {} at date: {}", 
								differenceInDays, stock.getCompanyName(), priceHist.get(priceHistPosition).getShortDateString());
					signalFound = true;
				}
				// Bearish zero line crossover
				else if (macdValues[priceHistPosition] < 0 && macdValues[priceHistPosition -1] >= 0) {

					stock.addSignal(new Signal(getFilterName(), 
							priceHist.get(priceHistPosition).getDate(), 
							differenceInDays + " day(s) old MACD zero line crossover SELL signal for " + stock.getCompanyName() + 
							" at date: " + priceHist.get(priceHistPosition).getShortDateString() + " with closing price:" + 
							priceHist.get(priceHistPosition).getClosingPrice(),
							SignalType.SELL));
					
					
					log4j.debug("{} day(s) old. MACD zero line crossover SELL signal for {} at date: {}", 
								differenceInDays, stock.getCompanyName(), priceHist.get(priceHistPosition).getShortDateString());
					signalFound = true;
				}

				priceHistPosition--;

			} while (priceHistPosition > 0 && signalFound == false && continueUntilSignalFound == true);
		}
		else {
			log4j.error("MACD, Not long enough price history to calculate EMA for: {}-day period found: {} days", 
						slowEMALength, (priceHist.size() -1));
		}
	} // end applyFilter()

	@Override
	public void backTrade(Calendar argStartDate, Stock argStock, int argInitStopLoss, int argTrailStopLoss) {
		stock = argStock;
		priceHist = stock.getPrices();
		
		// Make first sure we have data for at least one calculation of MACD.
		// Add + 1 to longEMA since we have to be able to calculate yesterdays EMA, i.e. EMA(y) in the formula.
		// EMA = Price(t) * k + EMA(y) * (1 – k) where EMA(y) is yestedays EMA.
		if (priceHist.size() > (slowEMALength + 1) ) {

			float byingPrice = 0.0f;
			float totalWinnings = 0.0f;

			macdValues = calcMACDValues();
			signalLineValues = calcSignalLine();
			
			
			int idx = slowEMALength + 1;
			
			if (argStartDate != null) {
				idx = checkStartDate(argStartDate);
			}
			if (idx > -1) {
				while (idx < priceHist.size()) {
					
					// Bullish signal line crossover
					if (macdValues[idx] > signalLineValues[idx] &&
							macdValues[idx -1] <= signalLineValues[idx -1]) {
						
						// If the stock is currently not held, BUY it if the BUY signal is given and the stock is in a DOWNTREND
						if (byingPrice == 0.0f) {
							byingPrice = priceHist.get(idx).getHighestPrice();
							
							log4j.debug("Bullish signal line crossover found on {}", priceHist.get(idx).getShortDateString());
							log4j.debug("MACD: {}", macdValues[idx]);
							log4j.debug("Signal line: {}", signalLineValues[idx]);
						}
					}
					// Bearish signal line crossover
					else if (macdValues[idx] < signalLineValues[idx] &&
							macdValues[idx - 1] >= signalLineValues[idx -1]) {
						
						if (byingPrice != 0.0f) {
							totalWinnings += priceHist.get(idx).getLowestPrice() - byingPrice;
							byingPrice = 0.0f;

							log4j.debug("Bearish signal line crossover found on {}", priceHist.get(idx).getShortDateString());
							log4j.debug("MACD: {}", macdValues[idx]);
							log4j.debug("Signal line: {}", signalLineValues[idx]);
						}
					}
					
					// Bullish zero line crossover
					if (macdValues[idx] > 0 && macdValues[idx -1] <= 0) {
						
						// If the stock is currently not held, BUY it if the BUY signal is given and the stock is in a DOWNTREND
						if (byingPrice == 0.0f) {
							byingPrice = priceHist.get(idx).getHighestPrice();
							
							log4j.debug("Bullish zero line crossover found on {}", priceHist.get(idx).getShortDateString());
							log4j.debug("MACD: {}", macdValues[idx]);
							log4j.debug("Signal line: {}", signalLineValues[idx]);
						}
					}
					// Bearish zero line crossover
					else if (macdValues[idx] < 0 && macdValues[idx -1] >= 0) {

						if (byingPrice != 0.0f) {
							totalWinnings += priceHist.get(idx).getLowestPrice() - byingPrice;
							byingPrice = 0.0f;

							log4j.debug("Bearish zero line crossover found on {}", priceHist.get(idx).getShortDateString());
							log4j.debug("MACD: {}", macdValues[idx]);
							log4j.debug("Signal line: {}", signalLineValues[idx]);
						}
					}
					// If the initial stoploss triggers, sell the stock
					if (byingPrice != 0.0f && priceHist.get(idx).getClosingPrice() < (byingPrice * (1 - ((float)argInitStopLoss / 100))) ) {
						
						totalWinnings += priceHist.get(idx).getClosingPrice() - byingPrice;

						log4j.debug("Stop loss triggered, last buy/sell result: {} - {} = {}",byingPrice, 
									 priceHist.get(idx).getClosingPrice(), priceHist.get(idx).getClosingPrice() - byingPrice);
						log4j.debug("MACD Stop loss triggered for {} at date: {} with price: {}", stock.getCompanyName(), 
									priceHist.get(idx).getShortDateString(), priceHist.get(idx).getClosingPrice());

						byingPrice = 0.0f;
					}
					idx++;
				}
				
				log4j.info("Total winning price: " + totalWinnings);
			}
			else {
				log4j.error("Not long enough price history to calculate EMA for: {}-day period found: {} days", 
							slowEMALength, (priceHist.size() -1));
			}
		}
	} // end backTrade()

	
	/**
	 * Calculates an Exponential Moving Average (EMA for a series of closing prices of a stock. A shortcoming
	 * is that the first days of the price history up to the first day after the length of emaPeriod
	 * is 0 since a EMA cannot be calculated for a series shorter than the selected n-day period (emaPeriod).
	 * 
	 * @param emaPeriod an int that is the number of days in the period
	 * 
	 * @return an array of float that holds the calculated EMA values (for the selected n-day period) for the closing price history of a stock.
	 */
	private float[] calcExponentialMovingAverages(int emaPeriod) {
		
		float[] emaArray = new float[priceHist.size() - 1];
		
		// First, calculate simple moving average for first n-day period, to use it for EMA(y) (yesterdays EMA-value) 
		// in the first iteration of EMA calculations in the formula EMA = Price(t) * k + EMA(y) * (1 – k)
		// where t = today, y = yesterday, N = number of days in EMA, k = 2/(N+1).
		// This is because we cannot define EMA(y) for the first period without this, i.e. we have to 
		// start somewhere.
		
		float smaSum = 0.0f;
		
		for (int position = 0; position < emaPeriod; position++) {
			smaSum += priceHist.get(position).getClosingPrice();
		}

		float sma = smaSum / emaPeriod;
		emaArray[emaPeriod -1] = sma;
		
		for (int position = emaPeriod; position < (priceHist.size() - 1); position++) {
			float k = 2 / ((float)emaPeriod + 1);
			emaArray[position] = priceHist.get(position).getClosingPrice() * k + emaArray[position -1] * (1-k);
		}
		
		return emaArray;
	} // end calcExponentialMovingAverages()
	
	/**
	 * Calculates signal line values for the MACD. The signal line is calculated as a 9-day EMA of MACD values
	 * for the selected stock.
	 * 
	 * @return an array of floats that holds the signal line values
	 */
	private float[] calcSignalLine() {
		
		float[] signalLineValues = new float[priceHist.size()];
		int position = 0;

		// First, calculate simple moving average for first period, to use it for EMA(y) in the first 
		// iteration of EMA calculations in the formula EMA = Price(t) * k + EMA(y) * (1 – k)
		// where t = today, y = yesterday, N = number of days in EMA, k = 2/(N+1).
		// This is because we cannot define EMA(y) for the first period without this, i.e. we have to 
		// start somewhere.
		float smaSum = 0.0f;
		
		while (position < signalLineLength) {
			smaSum += macdValues[position];
			position++;
		}

		float sma = smaSum / signalLineLength;
		signalLineValues[position] = sma;
		position++;
		
		// With the first iteration finished (i.e. the first position after number of days for 
		// Calculate the EMA for all days with the forumula EMA = Price(t) * k + EMA(y) * (1 – k)
		while (position < (macdValues.length - 1)) {

			float k = 2 / ((float)signalLineLength + 1);
			signalLineValues[position] = macdValues[position] * k + signalLineValues[position -1] * (1-k);
			position++;
		}
		
		return signalLineValues;
	} // end calcSignalLine()
	
	/**
	 * Calculates MACD values for the closing price history of the stock. MACD is defined as
	 * MACD = fast EMA - slow EMA.
	 * 
	 * @return an array of floats that are MACD values.
	 */
	private float[] calcMACDValues() {
	
		float[] fastEmaArray = calcExponentialMovingAverages(fastEMALength);
		float[] slowEmaArray = calcExponentialMovingAverages(slowEMALength);
		float[] macds 		 = new float[priceHist.size()];
		
		for (int position = slowEMALength + 1; position < priceHist.size() - 1; position++) {
			macds[position] = fastEmaArray[position] - slowEmaArray[position]; 
		}
		
		return macds;
	} // end calcMACDValues()
	
} // end class MACDFilter
