package com.stocktastic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.stocktastic.pojo.PriceListing;
import com.stocktastic.pojo.Stock;

/**
 * Class that handles Stock objects. Creating Stock objects
 * from file, return Stock collections etc.
 * 
 * @author stationaradm
 */
public class StockHandler implements IStockHandler {
	
	// File that contains ticker information.
	private File tickerFile;
	
	// Text file that contains collections of files.
	private File stockCollectionFile;
	
	private BufferedReader buffReader;
	
	// Map that contains the name of the company as key and
	// the symbol name as a value.
	private Map<String, String> stockNameMap;
	
	private Map<String, Map<String, String>> stockCollectionMap;
	
	private String priceHistoryLocation;
	
	/**
	 * Constructor for class StockHandler
	 * 
	 * @param tickerFilePath a String that is the path to a textfile containing ticker symbol names
	 * @param stockCollectionFile a String that is a path to a file containing stock collections
	 */
	public StockHandler(String tickerFilePath, String stockCollectionFile, String priceHistoryLocation) {
		stockNameMap = new HashMap<String, String>();
		stockCollectionMap = new HashMap<String, Map<String,String>>();
		this.tickerFile = new File(tickerFilePath);
		this.stockCollectionFile = new File(stockCollectionFile);
		this.priceHistoryLocation = priceHistoryLocation;
	} // end constructor StockHandler()


	/**
	 * Loads the ticker file containing all the company names mapped
	 * to symbol names.
	 */
	public void loadTickerFile() {

		try {
			buffReader = new BufferedReader(new FileReader(tickerFile));
			String priceLine = null;
			String[] tickerArray = null;
			
			while ((priceLine = buffReader.readLine()) != null) {
				
				// Delimeter in downloaded file is 1 Space + Tab for some reason.
				tickerArray = priceLine.split("	");
				stockNameMap.put(tickerArray[1], tickerArray[2]);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.format("The file containing tickers is not available: %s", tickerFile.getAbsolutePath() );
		}
		catch (IOException e) {
			System.out.format("Failed to read ticker file: %s", tickerFile.getAbsolutePath());
		}
		finally {
			try {
				if (buffReader != null) {
					buffReader.close();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	} // end loadTIckerFile()
	
	
	/**
	 * Retrieves matches to each occurence of the search words list given 
	 * as argument to the method. Each search word is compared with regular 
	 * expression to the existing corporation names.
	 * 
	 * @param searchedStocks a List of Strings which hold corporate names
	 * @return a List of stocks that match the given search words.
	 */
	public List<String> findStocks(List<String> searchedStocks) {
		
		List<String> foundStocksList = new ArrayList<String>();
		
		for (String searchWord : searchedStocks) {
			for (String currentKey : stockNameMap.keySet()) {
				if (currentKey.matches("(?i:.*" + searchWord + ".*)")) {
					foundStocksList.add(currentKey);
				}
			}
		}
		
		return foundStocksList;
	} // end findStocks()
	
	/**
	 * Returns all available stocks for download
	 * 
	 * @return a List with the name of all stocks available
	 */
	public List<String> getAllStocks() {

		List<String> allStocksList = new ArrayList<String>();
		
		for (String currentKey : stockNameMap.keySet()) {
			allStocksList.add(currentKey);
		}
		
		return allStocksList;
	} // end getAllStocks()
	
	/**
	 * Prints all the tickers available.
	 */
	public void printTickerList() {
		
		Map<String, String> sortedStockMap = new TreeMap<String, String>(stockNameMap);
		
		for (Entry<String, String> entrySet : sortedStockMap.entrySet()) {
			System.out.format("%-40s : %s%n", entrySet.getKey(), entrySet.getValue());
		}
		
	} // end printTickerList()
	

	/**
	 * Returns a single stock with the specified name
	 * 
	 * @param stockName a String that is the ticker name for the stock
	 * @return a Stock object that represents the selected stock
	 */
	public Stock getStock(String stockName) {
		
		Stock selectedStock = null;
		boolean chosenStocksOk = true;
		BufferedReader inReader = null;
		// Check that the stock exists
			
		if (!stockNameMap.containsKey(stockName)) {
			chosenStocksOk = false;
			System.out.format("The stock with name %s could not be found%n! Please try again!", stockName);
		}
		else {
			if (System.getenv("DEBUG").equals("true")) {
				System.out.format("Stock %s with symbol %s found!%n", stockName, stockNameMap.get(stockName));
			}
			selectedStock = new Stock(stockName, stockNameMap.get(stockName));
			File priceHistoryFile = new File(priceHistoryLocation + stockNameMap.get(stockName) + ".csv");
			ArrayList<PriceListing> priceListings = new ArrayList<PriceListing>();
			
			if (priceHistoryFile.exists()) {
				
				try {
					inReader = new BufferedReader(new FileReader(priceHistoryFile));
					String inLine = null;
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

					while ((inLine = inReader.readLine()) != null) {
						
						if (!inLine.matches("^Date.*")) {
							String[] splitLine = inLine.split(",");
							priceListings.add(new PriceListing(dateFormat.parse(splitLine[0]), 
											  Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), 
											  Float.parseFloat(splitLine[3]), Float.parseFloat(splitLine[4]), 
											  Float.parseFloat(splitLine[6]), Integer.parseInt(splitLine[5])));
						} 
					}
					Collections.sort(priceListings);
					selectedStock.setPrices(priceListings);
				} 
				catch (ParseException e) {
					System.out.format("There was a problem when parsing the data read from file. "
							+ " Please check the source file for the selected stock. File location: %s%n",
							priceHistoryFile);
				}
				catch (IOException e) {
					System.out.format("There was an IO-problem when reading the price data file: %s%n", 
									  priceHistoryFile);
				}
				finally {
					if (inReader != null) {
						try {
							inReader.close();
						} 
						catch (IOException e) {
							e.printStackTrace();
						}
						
					}
				}
			}
		}
		
		return selectedStock;
	} // end getStock()
	
	
	/**
	 * Returns a list of stocks with the supplied names
	 * 
	 * @return a List of stocks that has been fetched from file.
	 */
	public List<Stock> getStocks(List<String> stockNames) {
		
		List<Stock> stockList = new ArrayList<Stock>();
		boolean chosenStocksOk = true;
		BufferedReader inReader = null;
		// Check that the stock exists
		for (String stockName : stockNames) {
			
			if (!stockNameMap.containsKey(stockName)) {
				chosenStocksOk = false;
				System.out.println("The stock could not be found! Please try again!");
				break;
			}
			else {
				System.out.format("Stock %s with symbol %s found!%n", stockName, stockNameMap.get(stockName));
				Stock currentStock = new Stock(stockName, stockNameMap.get(stockName));
				File priceHistoryFile = new File(priceHistoryLocation + stockNameMap.get(stockName) + ".csv");
				ArrayList<PriceListing> priceListings = new ArrayList<PriceListing>();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

				try {
					inReader = new BufferedReader(new FileReader(priceHistoryFile));
					String inLine = null;
				
					while ((inLine = inReader.readLine()) != null) {
						
						if (!inLine.matches("^Date.*")) {
							String[] splitLine = inLine.split(",");
							priceListings.add(new PriceListing(dateFormat.parse(splitLine[0]), 
											  Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), 
											  Float.parseFloat(splitLine[3]), Float.parseFloat(splitLine[4]), 
											  Float.parseFloat(splitLine[6]), Integer.parseInt(splitLine[5])));
						} 
					}
					Collections.sort(priceListings);
					Collections.reverse(priceListings);
					currentStock.setPrices(priceListings);
					stockList.add(currentStock);
				} 
				catch (NumberFormatException e) {
					e.printStackTrace();
				} 
				catch (ParseException e) {
					e.printStackTrace();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					if (inReader != null) {
						try {
							inReader.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return stockList;
		
	} // end getStocks()
	
	// TODO Method that searches for a specific stock.
	
	
	/**
	 * Loads all stock collections (user defined and official, e.g. OMXS30) into 
	 * the stock handler. The collections are stored in a regular text file separated
	 * by ','.
	 */
	public void loadStockCollections() {
		
		try {
			buffReader = new BufferedReader(new FileReader(stockCollectionFile));
			String stockLine = null;
			String[] stockLineArray = null;
			
			while ((stockLine = buffReader.readLine()) != null) {
				
				stockLineArray = stockLine.split(",");
				
				// Add a new collection if not present in the Map.
				if ( !stockCollectionMap.containsKey(stockLineArray[0]) ) {
					stockCollectionMap.put(stockLineArray[0], new HashMap<String, String>());
				}
				
				stockCollectionMap.get(stockLineArray[0]).put(stockLineArray[1], stockLineArray[2]);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.format("The file containing stock collections is not available: %s", 
					StockTastic.STOCKCOLLECTIONFILE );
		}
		catch (IOException e) {
			System.out.format("Failed to open stock collection file: %s", StockTastic.STOCKCOLLECTIONFILE);
		}
		finally {
			try {
				buffReader.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	} // end loadStockCollections()
	
	
	/**
	 * Checks if the specified collection can be found in the map.
	 * 
	 * @param collectionName a String that is the specified collection name
	 * @return a boolean that is true if the specified collection can be found
	 */
	public boolean containsCollection(String collectionName) {
		
		boolean contains = false;
		
		if (stockCollectionMap.containsKey(collectionName)) {
			contains = true;
		}
		
		return contains;
	} // end containsCollection()
	
	/**
	 * Returns a map of all individual stock components of the specified list.
	 * 
	 * @param collectionName a String that specifies the stock collection (e.g. OMXS30)
	 * @return a Map containing company name as key and stock symbol as value
	 */
	public Map<String, String> getStockCollection(String collectionName) {
		return stockCollectionMap.get(collectionName);
	} // end getStockCollection()
	
} // end class StockHandler
