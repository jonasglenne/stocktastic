package com.stocktastic;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.stocktastic.filter.StockFilter;
import com.stocktastic.pojo.Index;
import com.stocktastic.pojo.Signal;
import com.stocktastic.pojo.Stock;
import com.stocktastic.pojo.Signal.SignalType;

public class StockTastic {
	
	public static String PRICEHISTORYLOCATION;
	public static String TICKERFILE;
	public static String INDEXFILE;
	public static String TICKERSSOURCEFILE;
	public static String STOCKCOLLECTIONFILE;
	public static String FILTERFILENAME;
	public static String FILTERCOLLECTIONFILE;
	public static String CLOSEDDAYSSTOCKEXCHANGE;
	public static String LOG4JCONFIGFILE;
	
	private static Logger log4j;
	
	public static void main (String[] args) {
		
		List<String> argsList = Arrays.asList(args);
		
		// Set the config file for log4j and initialize the logger
		System.setProperty("log4j.configurationFile", "config/log4j2.xml");
		log4j = LogManager.getLogger(StockTastic.class.getName());

		Properties properties = new Properties();
		
		// Contains a number of days when the stock exchange is open half the day or not at 
		// all (days that are not saturday or sunday)
		List<String> exchangeClosedDays = new ArrayList<String>();
		
		Calendar startDateCal = null;
		
		// The maxium days back in time we are interested in signals.
		// Set through command line argument otherwise we are interested
		// a long time back. Default is 1 days old signals.
		int signalOldestDays = 1;
		
		try {
		  properties.load(new FileInputStream("config/config.properties"));
		  
		  	PRICEHISTORYLOCATION = properties.getProperty("pricehistory");
			TICKERFILE = properties.getProperty("tickerfile");
			INDEXFILE = properties.getProperty("indexfile");
			TICKERSSOURCEFILE = properties.getProperty("stockcollectionfile");
			STOCKCOLLECTIONFILE = properties.getProperty("stockcollectionfile");
			FILTERFILENAME = properties.getProperty("filterfile");
			FILTERCOLLECTIONFILE = properties.getProperty("filtercollectionfile");
			CLOSEDDAYSSTOCKEXCHANGE = properties.getProperty("stockexchclosedfile");
			LOG4JCONFIGFILE = properties.getProperty("log4j.configurationFile");
			// Make sure the data directory and all the necessary data files are in place 
			if (!(new File(PRICEHISTORYLOCATION)).exists()) {
				new File(PRICEHISTORYLOCATION).mkdir();
			}
			
			if(!(new File(TICKERFILE)).exists()) {
				try {
					new File(TICKERFILE).createNewFile();
					//TODO Make a method that handles downloading the complete ticker file.
					// properties.getProperty("tickerssourcefile")
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (!(new File(INDEXFILE).exists())) {
				try {
					new File(INDEXFILE).createNewFile();
					//TODO Make a method that handles downloading the complete ticker file.
					// properties.getProperty("tickerssourcefile")
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(!(new File(STOCKCOLLECTIONFILE)).exists()) {
				try {
					new File(STOCKCOLLECTIONFILE).createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(!(new File(FILTERFILENAME)).exists()) {
				try {
					new File(FILTERFILENAME).createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(!(new File(FILTERCOLLECTIONFILE)).exists()) {
				try {
					new File(FILTERCOLLECTIONFILE).createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(!(new File(CLOSEDDAYSSTOCKEXCHANGE)).exists()) {
				try {
					new File(CLOSEDDAYSSTOCKEXCHANGE).createNewFile();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				BufferedReader bufferReader = null;
				
				try {
					bufferReader = new BufferedReader(new FileReader(CLOSEDDAYSSTOCKEXCHANGE));
					String line = null;
					while ((line = bufferReader.readLine()) != null) {
						exchangeClosedDays.add(line);
					}
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				finally {
					try {
						if (bufferReader != null) {
							bufferReader.close();
						}
					} 
					catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		// Create a Download Handler.
		DownloadHandler downloadHandler = new DownloadHandler();
		
		// Fire up a Stock Handler and load the ticker information.
		StockHandler stockHandler = new StockHandler(TICKERFILE, STOCKCOLLECTIONFILE, PRICEHISTORYLOCATION);
		stockHandler.loadTickerFile();
		stockHandler.loadStockCollections();
		
		// Fire up an Index handler and load the index information.
		IndexHandler indexHandler = new IndexHandler(INDEXFILE, PRICEHISTORYLOCATION);
		indexHandler.loadIndexFile();
		
		FilterHandler filterHandler = new FilterHandler(FILTERFILENAME, FILTERCOLLECTIONFILE);
		
		// Collect the stocks selected into a name list. The different parts of the software
		// handles only stock names and loads Stock instances as needed. Moving around a large
		// list is deemed to memory intensive.
		List<String> selectedStocksList = new ArrayList<String>();
		
		// ============================================================================================================
		// Show the help text for this software
		// ============================================================================================================
		if (argsList.contains("-h") || argsList.contains("--help")) {
			printHelp();
		}
		
		// ============================================================================================================
		// Finds a specified stock or filter.
		// ============================================================================================================
		if (argsList.contains("--find")) {
			int position = (argsList.indexOf("--find") + 1);
			List<String> searchTerms = new ArrayList<String>();

			while ( (position < argsList.size()) && 
					!argsList.get(position).startsWith("-") ) {
				searchTerms.add(argsList.get(position));
				position++;
			}
			
			log4j.info("Searching for stocks that match the following names: ");
			for (String currentWord : searchTerms) {
				log4j.info(currentWord);
			}
			
			List<String> foundStocks = stockHandler.findStocks(searchTerms);
			Collections.sort(foundStocks);
			
			if (foundStocks.size() == 0) {
				log4j.info("Done. Found no stocks with given argument");
			}
			else {
				log4j.info("Done. Found the following stocks:");
				
				for (String stockName : foundStocks) {
					log4j.info(stockName);
				}
			}
		}
		
		// ============================================================================================================
		// Finds a specified stock list or filter.
		// ============================================================================================================
		if (argsList.contains("--list")) {
			stockHandler.printTickerList();
		}

		// ============================================================================================================
		// IF      Check and add the stocks the user has selected to stockList
		// ELSE If no stocks has been selected, use all available stocks
		// ============================================================================================================
		if (argsList.contains("--stocks")) {

			int argPosition = (argsList.indexOf("--stocks") + 1);
			
			while ( (argPosition < argsList.size()) && !argsList.get(argPosition).startsWith("-") ) {
				
				// IF:      If the user has specified ALL, then load all stocks available in the ticker file.
				// ELSE:    Add the specified stock
				if (argsList.get(argPosition).equalsIgnoreCase("alla")) {
					
					List<String> allStocks = stockHandler.getAllStocks();
					
					for(String currentStock : allStocks) {
						selectedStocksList.add(currentStock);
					}
				}
				else {
					selectedStocksList.add(argsList.get(argPosition));
				}

				argPosition++;
			}

			log4j.debug("Selected stocks:");
			Iterator<String> stockIter = selectedStocksList.iterator();
			while (stockIter.hasNext()) {
				log4j.debug(stockIter.next());
			}
		}
		else {
			selectedStocksList.addAll(stockHandler.getAllStocks());
		}

		// ============================================================================================================
		// Updating of stock prices and indices selected.
		// ============================================================================================================
		if (argsList.contains("--update")) {

			// Keeps track of the latest downloaded date and the current date.
			Calendar latestDownloadCal = new GregorianCalendar();
			Calendar todayCal = new GregorianCalendar();
			todayCal.set(Calendar.HOUR_OF_DAY, 0);  
			todayCal.set(Calendar.MINUTE, 0);  
			todayCal.set(Calendar.SECOND, 0);  
			todayCal.set(Calendar.MILLISECOND, 0);
			Date currentDate = todayCal.getTime();
			
			// Always download all the selected indices, no command line option necessary.
			List<String> indices = indexHandler.getAllIndices();
			
			for (String currentIndexName : indices) {
				
				Index currentIndex = indexHandler.getIndex(currentIndexName);
				Date latestDownloadDate = currentIndex.getLatestDownloadedDate();
				boolean closedDay = StockTasticUtils.checkExchangeClosed(exchangeClosedDays, currentDate);
				
				// IF:      No prices downloaded yet. Download everything.
				// ELSE IF: Price history exists but is not up to date, download missing history.
				// ELSE:    Price history is up to date.
				if (latestDownloadDate == null) {

					latestDownloadCal.set(1995, 0, 1);
					downloadHandler.downloadIndexHistoryAndQuote(currentIndex.getSymbol(), latestDownloadCal, todayCal);
					log4j.info("No prices downloaded yet for {}. Download everything up today...", currentIndex.getIndexName());
				}
				else if (currentDate.after(latestDownloadDate)) {
					
					latestDownloadCal.setTime(latestDownloadDate);
					long noOfMissingDays = StockTasticUtils.calcDateDayDifference(currentDate, latestDownloadDate);

					// If it is saturday or sunday and we already have price info up to friday the same week, skip download.
					// Also, if the exchange is closed the whole day today (e,g, bank holiday) and we have until yesterday in
					// price history, do not download
					if( (todayCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && noOfMissingDays == 1) || 
					    (todayCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && noOfMissingDays < 3) ||
					    (noOfMissingDays == 1 && closedDay == true) ) {
						log4j.debug("Price history for {} already up to date", currentIndex.getIndexName());
					}
					else {
						// If only today's price quote is neeeded, download today's price quote. Also, if the latest available 
						// price history is from friday last week and today is monday (which would give a difference of 
						// 4 days, only download monday's price quote
						if ( (todayCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY && 
							(latestDownloadCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY && noOfMissingDays < 7) ) ||
							noOfMissingDays == 1) {
							downloadHandler.downloadIndexPriceQuote(currentIndex.getSymbol());
							log4j.debug("Fetching price quote update for the today for {}...", 1, currentIndex.getIndexName());
						}
						else {
							// If there are several day's to download and also today's quote is needed. Make a download from historic prices
							// and also add today's price quote.
							latestDownloadCal.add(Calendar.DAY_OF_MONTH, 1);
							downloadHandler.downloadIndexHistoryAndQuote(currentIndex.getSymbol(), latestDownloadCal, todayCal);
							log4j.debug("Fetching price history update for the {} latest days for {}...", noOfMissingDays, currentIndex.getIndexName());
						}
					}
				}
				else {
					log4j.debug("Price history has already been updated for {} to current date already, only updating latest quote", 
							currentIndex.getIndexName());
					downloadHandler.downloadIndexPriceQuote(currentIndex.getSymbol());
				}
			}
			
			// Download the selected stocks
			int index = 0;
			
			while (index < selectedStocksList.size()) {
				
				Stock currentStock = stockHandler.getStock(selectedStocksList.get(index));
				
				if (currentStock != null) {
					
					Date latestDownloadDate = currentStock.getLatestDownloadedDate();
					boolean closedDay = StockTasticUtils.checkExchangeClosed(exchangeClosedDays, currentDate);
					
					// IF:   No prices downloaded yet. Download everything.
					// ELSEIF: Price history exists but is not up to date, download missing history.
					// ELSE: Price history exists and is up to date, update quote only.
					if (latestDownloadDate == null) {

						latestDownloadCal.set(1995, 0, 1);
						downloadHandler.downloadStockPriceHistoryAndQuote(currentStock.getSymbol(), latestDownloadCal, todayCal);
						log4j.info("No prices downloaded yet for {}. Download everything up today...", currentStock.getCompanyName());
					}
					else if (currentDate.after(latestDownloadDate)) {
						
						latestDownloadCal.setTime(latestDownloadDate);
						long differenceInDays = StockTasticUtils.calcDateDayDifference(currentDate, latestDownloadDate);
	
						// If it is saturday or sunday and we already have price info up to friday the same week, skip download.
						// Also, if the exchange is closed the whole day today (e,g, bank holiday) and we have until yesterday in
						// price history, do not download
						if( (todayCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && differenceInDays == 1) || 
						    (todayCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && differenceInDays < 3) ||
						    (differenceInDays == 1 && closedDay == true) ) {
							log4j.debug("Price history for {} already up to date", currentStock.getCompanyName());
						}
						else {
							
							// If this is the monday and we have prices until friday, download only the price quote for today
							// else download the full history + today's price quote.
							if ( (todayCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY && 
								 (latestDownloadCal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY  && differenceInDays < 7) ) ||
								differenceInDays == 1) {
								downloadHandler.downloadStockPriceQuote(currentStock.getSymbol());
								log4j.debug("Fetching price quote update for the today for {}...", 1, currentStock.getCompanyName());
							}
							else {
								latestDownloadCal.add(Calendar.DAY_OF_MONTH, 1);
								downloadHandler.downloadStockPriceHistoryAndQuote(currentStock.getSymbol(), latestDownloadCal, todayCal);
								log4j.debug("Fetching price history update for the {} latest days for {}...", differenceInDays, currentStock.getCompanyName());
							}
						}
					}
					else {
						log4j.debug("Price history has already been updated for {} to current date already, only updating latest quote", 
								    currentStock.getCompanyName());
						downloadHandler.downloadStockPriceQuote(currentStock.getSymbol());
					}
				}
				index++;
			}
			downloadHandler.shutDownHandlerAndWait();
		}


		// ============================================================================================================
		// Applying a filter to selected stocks.
		// ============================================================================================================
		if (argsList.contains("--filters")) {
			
			long startTime = System.currentTimeMillis();
			
			List<String> filterList = new ArrayList<String>();
			int filterListIdx = 1;
			int index = argsList.indexOf("--filters");

			while ( (index + filterListIdx < args.length) && 
					!(args[index + filterListIdx].startsWith("-"))) {
				filterList.add(args[index + filterListIdx]);
				filterListIdx++;
			}
			
			log4j.debug("Selected filters:");
			for (String filter : filterList) {
				log4j.debug(filter);
			}

			// This argument determines the type of filter search we want to do.
			// Either we are looking for signals or we are backtrading the filter 
			// to see if it is effective for the chosen stock.
			if (argsList.contains("--type")) {
				
				int typeArgIndex = argsList.indexOf("--type") + 1;
				String filterApplicationType = argsList.get(typeArgIndex);
				log4j.debug("Selected filter application type: {}", filterApplicationType);
				
				if (filterApplicationType.equalsIgnoreCase("signal")) {
					
					// This argument determines the oldest signal we are interested in in terms
					// of days, i.e. if a sell signal is six days old and days is set to 5, the
					// signal will not be reported.
					if (argsList.contains("--days")) {
						int daysArgIndex = argsList.indexOf("--days") + 1;
						signalOldestDays = Integer.parseInt(argsList.get(daysArgIndex));
						log4j.debug("Selected number of days in history to report signal: {}", signalOldestDays);
					}
					
					List<Stock> stocksWithSignals = new ArrayList<Stock>();
					
					for (String stock : selectedStocksList) {
						
						Stock currentStock = stockHandler.getStock(stock);
						
						if (currentStock != null) {
							
							for (String filter : filterList) {
								
								StockFilter currentFilter = filterHandler.getFilter(filter);
								
								if (currentFilter != null) {
									
									log4j.debug("Applying filter {} on {}", filter, currentStock.getSymbol());
									currentFilter.applyFilter(currentStock, true);
								}
							}
							
							// Add the stock to the signal stock list if a signal was detected
							if (currentStock.getSignals().size() != 0) {
								
								if (stocksWithSignals.contains(currentStock)) {
									stocksWithSignals.remove(currentStock);
									stocksWithSignals.add(currentStock);
								}
								else {
									stocksWithSignals.add(currentStock);
								}
							}
						}
					}
					
					// Add stocks that had signals to a list.
					for (String stock : selectedStocksList) {
						
						Stock currentStock = stockHandler.getStock(stock);
						
						// Add the stock to the signal stock list if a signal was detected
						if (currentStock.getSignals().size() != 0) {
							
							if (stocksWithSignals.contains(currentStock)) {
								stocksWithSignals.remove(currentStock);
								stocksWithSignals.add(currentStock);
							}
							else {
								stocksWithSignals.add(currentStock);
							}
						}
					}
					
					// Run all filters on each stock if -inv was given on command line
					if (argsList.contains("--grouping")) {
						
						int grouping_index = argsList.indexOf("--grouping") + 1;
						String groupingString = argsList.get(grouping_index);

						if (groupingString.equalsIgnoreCase("stocks")) {
							
							log4j.info("----------------------");
							log4j.info("Grouping results on stocks");
							log4j.info("----------------------");

							// Report the results per stock (with a list of filters that generated signals for each stock).
							for (Stock stock : stocksWithSignals) {
								
								List<Signal> signals = stock.getSignals();

								StringBuilder buySignalString = new StringBuilder();
								StringBuilder sellSignalString = new StringBuilder();

								for (String filter : filterList) {
															
									for (Signal signal : signals) {
										
										// Filter our signals that are older than we have selected and that is not 
										// the current filter in the loop
										if (StockTasticUtils.calcDateDayDifference(StockTasticUtils.getCurrentDate(), signal.getDate()) <= signalOldestDays &&
											signal.getFilterName().equals(filter)) {

											if (signal.getBuySellType() == SignalType.BUY) {
												buySignalString.append(signal.getDescription() + "\n");
											}
											else {
												sellSignalString.append(signal.getDescription() + "\n");
											}
										}
									}
								}
								
								if (sellSignalString.length() > 0 || buySignalString.length() > 0) {
									log4j.info("----------------------");
									log4j.info("Stock: {}", stock.getCompanyName());
									log4j.info("----------------------");
								}
								if (buySignalString.length() > 0) {
									log4j.info("BUY signals");
									log4j.info(buySignalString);
								}
								if (sellSignalString.length() > 0) {
									log4j.info("SELL signals");
									log4j.info(sellSignalString);
								}								
							}
						}
						else if (groupingString.equalsIgnoreCase("filters")) {
							
							log4j.info("----------------------");
							log4j.info("Grouping results on filters");
							log4j.info("----------------------");
							
							// Report the results per filter where each stock that generated a signal for that filter is listed
							for (String filter : filterList) {
								
								log4j.info("----------------------");
								log4j.info("Filter: {}", filter);
								log4j.info("----------------------");
								
								StringBuilder buySignalString = new StringBuilder();
								StringBuilder sellSignalString = new StringBuilder();

								for (Stock stock : stocksWithSignals) {
									
									List<Signal> signals = stock.getSignals();
									
									for (Signal signal : signals) {
										
										if (StockTasticUtils.calcDateDayDifference(StockTasticUtils.getCurrentDate(), signal.getDate()) <= signalOldestDays &&
											signal.getFilterName().equals(filter)) {

											if (signal.getBuySellType() == SignalType.BUY) {
												buySignalString.append(signal.getDescription() + "\n");
											}
											else {
												sellSignalString.append(signal.getDescription() + "\n");
											}
										}
									}
								}
								
								if (!argsList.contains("-b")) {
									log4j.info("BUY signals");
									log4j.info(buySignalString);
									log4j.info("SELL signals");
									log4j.info(sellSignalString);
								}
							}
						}
						else {
							log4j.error("-------------------------------");
							log4j.error("Incorrect grouping argument (possible choices are 'stocks' or 'filters)', you selected: '{}'", groupingString);
							log4j.error("-------------------------------");
						}
					}
				}
				else if (filterApplicationType.equalsIgnoreCase("backtrade")) {
					
					if (selectedStocksList.size() > 1) {
						log4j.error("-------------------------------");
						log4j.error("Only selection of 1 stock is applicable for backtrade.");
						log4j.error("-------------------------------");
						System.exit(1);
					}
					if (filterList.size() > 1) {
						log4j.error("-------------------------------");
						log4j.error("Only selection of 1 filter is applicable for backtrade.");
						log4j.error("-------------------------------");
						System.exit(1);
					}

					Stock selectedStock = stockHandler.getStock(selectedStocksList.get(0));
					StockFilter selectedFilter = filterHandler.getFilter(filterList.get(0));

					// ============================================================================================================
					// If a specific start date has been selected, the analysis of the stocks should start from this date.
					// ============================================================================================================
					
					startDateCal = new GregorianCalendar();
					
					if (argsList.contains("--start")) {

						int startDateIndex = argsList.indexOf("--start") + 1;
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						
						try {
							startDateCal.setTime(dateFormat.parse(argsList.get(startDateIndex)));
						} 
						catch (ParseException e) {
							e.printStackTrace();
							//TODO Make this selectable from command line via argument -start
							startDateCal.set(Calendar.HOUR_OF_DAY, 0);  
							startDateCal.set(Calendar.MINUTE, 0);  
							startDateCal.set(Calendar.SECOND, 0);  
							startDateCal.set(Calendar.MILLISECOND, 0);
							startDateCal.set(Calendar.YEAR, 2003);
							startDateCal.set(Calendar.MONTH, 6); //January is 0!
							startDateCal.set(Calendar.DAY_OF_MONTH, 1); //1st day of month is 1!
						}
					}
					else {
						startDateCal.setTime(selectedStock.getPrices().get(0).getDate());
					}

					if (argsList.contains("--full")) {
						log4j.debug("Full backtrading selected on filter {}:", selectedFilter.getFilterName());
						selectedFilter.fullBackTrade(startDateCal, selectedStock, 2, 2);
					}
					else {
						log4j.debug("Backtrading selected on filter {}:", selectedFilter.getFilterName());
						selectedFilter.backTrade(startDateCal, selectedStock, 2, 2);
					}
				}
				else {
					log4j.error("-------------------------------");
					log4j.error("Incorrect application type argument (possible choices are 'signal' or 'backtrade)', you selected: '{}'", 
							    filterApplicationType);
					log4j.error("-------------------------------");
				}
				
				long endTime = System.currentTimeMillis();
				long delta = endTime - startTime;
				int minutes = (int) ((delta / 1000) / 60);
				int seconds = (int) ((delta / 1000) % 60);
				log4j.info("-------------------------------");
				log4j.info("Time spent filtering: {} minutes, {} seconds", minutes, seconds);
				log4j.info("-------------------------------");
			}
			else {
				log4j.error("No filter application type selected, select 'signal' or 'backtrade', e.g. '--type signal'.");
			}
		}
	} // end main()

	
	public static void printHelp() {
		System.out.print("StockTastic - Usage instructions\n\n");
		System.out.print("This sowftware can download stock prices from online, apply filters e.g. above MA50 \n"
						+ "and determine buy/sell signals for you. The following switches are availabe:\n\n"
						+ "--update                   The switch updates the specified stock(s) given in arg --stocks\n"
						+ "                           (see argument --stocks) before the filters are applied to them.\n"
						+ "                           To update all stocks at once, do not specify a --stocks argument.\n"
						+ "\n"
						+ "--list                     Prints a list of stock names in alphabetical order.\n"
						+ "\n"
						+ "--stocks <stock name>      This arguments specifies which stocks to operate on. To specify\n"
						+ "                           several stocks at once separate the different stocks with a \n"
						+ "                           space \" \" all stock names should be enclosed in double quotes.\n"
						+ "                           It is also possible to specify a prepared list of stock. For a list\n"
						+ "                           of stocks/index to chose from, use StockTastic -list stocks\n"
						+ "                           (see below)\n"
						+ "\n"
						+ "--filters <filter name>    This arguments specifies which filters to apply to the selected stock(s).\n"
						+ "                           Several filters at once can be selected by separating the different \n"
						+ "                           filters with a space. All filter names should be enclosed in double\n"
						+ "                           quotes \" \".\n"
						+ "                           It is also possible to specify a named list of filters.\n"
						+ "                           For a list of filters to choose from, use StockTastic -list <filter>\n"
						+ "\n"
						+ "--grouping <stocks|filters>Selects what to group the filter results on. If 'stocks' is selected.\n"
						+ "                           all filter signals for each stock are grouped together.\n"
						+ "                           If 'filters' is selected, all stocks with a signal will be listed for\n"
						+ "                           each selected filter.\n"
						+ "\n"
						+ "--days                     This argument determines the oldest signal we are interested in in terms\n"
						+ "                           of days, i.e. if a sell signal is six days old and days is set to 5, the\n"
						+ "                           signal will not be reported in the results.\n"
						+ "\n"
						+ "--backtrade                Backtrading. Back trade with selected filter(s). This means, start from\n"
						+ "                           first day in available price history and follow buy/sell using the\n"
						+ "                           selected filter. If --start argument is given, the filter will be used\n"
						+ "                           from the selected date.\n"
						+ "\n"
						+ "--start                    Used together with --backtrade to determine the start date in the price\n"
						+ "                           history to start applying the trades using the filter. E.g. if we want to\n"
						+ "                           se how the filter would work if we skip the IT bubble.\n"
						+ "\n"
						+ "--full                     This arguments specifies which filters to apply to the selected stock.\n"
						+ "\n"
						+ "\n"
						+ "Example 1:\n"
						+ ">StockTastic --download --stocks \"TIEN.ST\" \"SSAB-B.ST\" --filters \"MA50\" \"Stockhastic\"\n\n"
						+ "This command will first refresh the price history for Tieto (TIEN.ST) and SSAB B stocks\n"
						+ "and then apply an MA50 and stockhastic filter on the updated stock history. It will then\n"
						+ "print recommendations for those stocks (buy/sell/hold).");
	} // end printHelp()
	
} // end class StockTastic
