package com.stocktastic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.stocktastic.filter.IslandFilter;
import com.stocktastic.filter.KeyReversal;
import com.stocktastic.filter.MACDFilter;
import com.stocktastic.filter.MADoubleCrossOverFilter;
import com.stocktastic.filter.MAFilter;
import com.stocktastic.filter.MASlowBuyFastSell;
import com.stocktastic.filter.StockFilter;
import com.stocktastic.filter.StopLossChance;

/**
 * Class that handles Filter objects. Creating Filter objects
 * from file, return Filter collections etc.
 * 
 * @author stationaradm
 */
public class FilterHandler {
	
	// File that contains ticker information.
	private File filterFile;
	
	// Text file that contains collections of files.
	private File filterCollectionFile;
	
	private BufferedReader buffReader;
	
	// Map that contains the name of the filter as key and
	// a description of the filter as a value.
	private Map<String, String> filterNameMap;
	
	// Holds collections of filters. These can be predefined or user
	// defined. Simply holds a collection of filter.
	private Map<String, Map<String, String>> filterCollectionMap;
	
	private final Logger log4j;

	/**
	 * Constructor for class StockHandler
	 * 
	 * @param filterFilePath a String that is the path to a textfile containing ticker symbol names
	 * @param filterCollectionFilePath a String that is a path to a file containing stock collections
	 */
	public FilterHandler(String filterFilePath, String filterCollectionFilePath) {
		filterNameMap = new HashMap<String, String>();
		filterCollectionMap = new HashMap<String, Map<String,String>>();
		this.filterFile = new File(filterFilePath);
		this.filterCollectionFile = new File(filterCollectionFilePath);
		
		log4j = LogManager.getLogger(StockTastic.class.getName());

	} // end constructor StockHandler()


	/**
	 * Loads the ticker file containing all the company names mapped
	 * to symbol names.
	 */
	public void loadFilterFile() {
		
		try {
			buffReader = new BufferedReader(new FileReader(filterFile));
			String fitlerLine = null;
			String[] filterLineArray = null;
			
			while ((fitlerLine = buffReader.readLine()) != null) {
				
				// Delimeter in downloaded file is 1 Space + Tab for some reason.
				filterLineArray = fitlerLine.split(":");
				filterNameMap.put(filterLineArray[0], filterLineArray[1]);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.format("The file containing filters is not available: %s", StockTastic.TICKERFILE );
		}
		catch (IOException e) {
			System.out.format("Failed to read filter file: %s", StockTastic.TICKERFILE);
		}
		finally {
			try {
				buffReader.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	} // end loadFilterFile()
	
	
	/**
	 * Prints all the tickers available.
	 */
	public void printFitlerList() {
		
		Map<String, String> sortedFilterMap = new TreeMap<String, String>(filterNameMap);
		
		for (Entry<String, String> entrySet : sortedFilterMap.entrySet()) {
			System.out.format("Filter: %s%n Description: %s%n", entrySet.getKey(), entrySet.getValue());
		}
		
	} // end printTickerList()
	

	// TODO. Filters will have to be serialized or be part of the class hierarcy e.g
	//       MA50Filter extends Filter {
	/**
	 * Returns a single filter with the specified name
	 * 
	 * @param filterName a String that is the filter name
	 * @return a StockFilter object that represents the selected filter
	 */
	public StockFilter getFilter(String filterName) {
//		
		StockFilter selectedFilter = null;
		
		if (filterName.startsWith("MA Slow Buy Fast Sell")) {
			if (filterName.length() < 23) {
				log4j.error("You need to give three MA value in the form N/N/N e.g. 10/100/200");
			}
			else {
				String maPeriodString = filterName.substring(22, filterName.length());
				String[] maPeriods = maPeriodString.split("/");
				if (maPeriods.length < 3 || maPeriods.length > 3) {
					log4j.error("You need to give three MA value in the form N/N/N e.g. 10/100/200");
				}
				else {
					selectedFilter = new MASlowBuyFastSell(Integer.parseInt(maPeriods[0]), 
							   Integer.parseInt(maPeriods[1]),
							   Integer.parseInt(maPeriods[2]));
				}
			}
		}
		else if (filterName.startsWith("MA Double Crossover")) {
			String maPeriodString = filterName.substring(20, filterName.length());
			String[] maPeriods = maPeriodString.split("/");
			selectedFilter = new MADoubleCrossOverFilter(Integer.parseInt(maPeriods[0]), Integer.parseInt(maPeriods[1]));
		}
		else if (filterName.startsWith("MA Price Crossover")) {
			String maPeriodString = filterName.substring(19, filterName.length());
			selectedFilter = new MAFilter(Integer.parseInt(maPeriodString));
		}
		else if (filterName.equalsIgnoreCase("Key Reversal")) {
			selectedFilter = new KeyReversal();
		}
		else if (filterName.equalsIgnoreCase("Island Reversal")) {
			selectedFilter = new IslandFilter();
		}
		else if (filterName.equalsIgnoreCase("MACD")) {
			selectedFilter = new MACDFilter(26, 12, 9);
		}
		else if (filterName.equalsIgnoreCase("Stop Loss Chance")) {
			selectedFilter = new StopLossChance();
		}
		else {
			log4j.error("Selected filter: " + filterName + " not found:"); 
		}
	
		return selectedFilter;
	} // end getFilter()
	
	
	/**
	 * Returns a list of stocks with the supplied names
	 * 
	 * @return a List of stocks that has been fetched from file.
	 */
	public List<StockFilter> getFilters(List<String> filterNames) {
		
		List<StockFilter> filterList = new ArrayList<StockFilter>();
		
		return filterList;
		
	} // end getStocks()
	
	
	/**
	 * Loads all filter collections (user defined and official, e.g. MA50) into 
	 * the filter handler. The collections are stored in a regular text file separated
	 * by '	' (tab).
	 */
	public void loadFilterCollections() {
		
//		try {
//			buffReader = new BufferedReader(new FileReader(filterCollectionFile));
//			String filterLine = null;
//			String[] filterLineArray = null;
//			
//			while ((filterLine = buffReader.readLine()) != null) {
//				
//				filterLineArray = filterLine.split(",");
//				
//				// Add a new collection if not present in the Map.
//				if ( !filterCollectionMap.containsKey(filterLineArray[0]) ) {
//					filterCollectionMap.put(filterLineArray[0], new HashMap<String, String>());
//				}
//				
//				filterCollectionMap.get(stockLineArray[0]).put(stockLineArray[1], stockLineArray[2]);
//			}
//		} 
//		catch (FileNotFoundException e) {
//			System.out.format("The file containing stock collections is not available: %s", 
//					StockTastic.STOCKCOLLECTIONFILE );
//		}
//		catch (IOException e) {
//			System.out.format("Failed to open stock collection file: %s", StockTastic.STOCKCOLLECTIONFILE);
//		}
//		finally {
//			try {
//				buffReader.close();
//			}
//			catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
	} // end loadFilterCollections()
	
	
	/**
	 * Checks if the specified collection can be found in the map.
	 * 
	 * @param collectionName a String that is the specified collection name
	 * @return a boolean that is true if the specified collection can be found
	 */
	public boolean containsCollection(String collectionName) {
		
		boolean contains = false;
		
		if (filterCollectionMap.containsKey(collectionName)) {
			contains = true;
		}
		
		return contains;
	} // end containsCollection()
	
	/**
	 * Returns a map of all individual filter components of the specified list.
	 * 
	 * @param collectionName a String that specifies the stock collection (e.g. OMXS30)
	 * @return a Map containing filter name as key and a description as value
	 */
	public Map<String, String> getFilterCollection(String collectionName) {
		return filterCollectionMap.get(collectionName);
	} // end getStockCollection()
	
	
	/**
	 * Loads all available filters
	 */
	private void loadFilters() {
		
	}
	
} // end class StockHandler
